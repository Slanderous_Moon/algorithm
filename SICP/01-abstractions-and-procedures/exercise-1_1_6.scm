;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname exercise-1_1_6) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f () #f)))
;; SICP exercise 1.1.6 
;; Square root calculation with new-if.
(define (new-if predicate then-clause else-clause)
  (cond (predicate then-clause)
        (else else-clause)))

(define (sqrt-iter guess x)
  (new-if (good-enough? guess x)
          guess
          (sqrt-iter (improve guess x)
                     x)))


(define (improve guess x)
  (average guess (/ x guess)))


(define (average x y)
  (/ (+ x y) 2))


(define (square x) (* x x))


(define (good-enough? guess x)
  (< (abs (- (square guess) x)) 0.001))


(define (square-root x)
  (sqrt-iter 1.0 x))


;; main
(square-root 4)

;; The program never returns any value because
;; new-if is a function, and in Scheme Lisp, when a fuction
;; is called, the first thing that Scheme does is evaluation
;; of all the arguments. 