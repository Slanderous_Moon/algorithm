;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname 1_2_1-linear-recursion-and-linear-iteration) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f () #f)))
;; linear recursion

(define (factorial n)
  (if (= n 1)
      1
      (* n (factorial (- n 1)))))


;; linear iterations, 2 funcs

(define (factorial n)
  (fact-iter 1 1 ))


(define (fact-iter product counter max-count)
  (if (> counter max-count)
      product
      (fact-iter (* counter product)
                 (+ counter 1)
                 max-count)))


;; local naming iterations

(define (loc-factorial n)
  (define (loc-fact-iter product counter)
    (if (> counter )
        product
        (loc-fact-iter (* counter product)
                       (+ counter 1))))

  (loc-fact-iter 1 1))


;; Главное различие между линейно-рекурсивным и линейно-итеративным
;; процессами состоит в том, что линейно-рекурсивный процесс сначала
;; производит серию расширений (по мере построения цепочки отложенных
;; операций), а затем сжимается. Объём информации растёт линейно с ростом n.

;; В то же время линейно-итеративный процесс не растёт и не сжимается.
;; На каждом шаге достаточно помнить лишь текущие значения переменных.