;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname 1_2_2_tree_recursion) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f () #f)))
;; Размен одного доллара монетами по 50, 25, 10, 5, 1 центу

(define (count-change amount)
  (cc amount 5))


(define (cc amount kinds-of-coins)
  (cond ((= amount 0) 1)
        ((or (< amount 0) (= kinds-of-coins 0)) 0)
        (else (+ (cc amount
                     (- kinds-of-coins 1))
                 (cc (- amount
                        (first-denomination kinds-of-coins))
                     kinds-of-coins)))))


(define (first-denomination kinds-of-coins)
  (cond ((= kinds-of-coins 1) 1)
        ((= kinds-of-coins 2) 5)
        ((= kinds-of-coins 3) 10)
        ((= kinds-of-coins 4) 25)
        ((= kinds-of-coins 5) 50)))


;; main
(count-change 100)

;; Древовидная рекурсия неэффективна для вычисления конечного результата,
;; зато зачастую её гораздо проще сформулировать, чем итеративный процесс.
;; Если спроектировать компилятор, преобразующий древовидно-рекурсивные
;; процедуры в более эффективные, можно получить нечто прекрасное.