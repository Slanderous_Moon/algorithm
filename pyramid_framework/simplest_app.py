# Pyramid uses WSGI protocol to connect an application and web-server together
from wsgiref.simple_server import make_server

# An instance of Configurator is used to configure your Pyramid App
from pyramid.config import Configurator
# An instance of Response is used to create a web response
from pyramid.response import Response


def hello_world(request):
    """
    View callable function. doesn't need to be a function it can be
    represented via class or instance.
    :param request: request object
    :return: response object
    """
    return Response('Hello World!')


if __name__ == '__main__':
    # config obj. represents API which script uses to configure this
    # particular Pyramid app.
    with Configurator() as config:
        # This method registers a route to the root (/) URL path.
        config.add_route('hello', '/')
        # Registers hello_world as view_callable
        config.add_view(hello_world, route_name='hello')
        # return WSGI app object
        app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 6543, app)
    server.serve_forever()


# View callable
"""
Callable Python object, associated with a view configuration. Returns
Response object. accepts single argument, request, which is an instance
of a Request object.
An alternate calling convention allows a view to be defined as a callable
which accepts a pair of arguments - context and request. This convention 
is useful for traversal-based applications in which context is very important.
View callable is the primary mechanism by which a developer writes user
interface code within Pyramid.

A view callable is required to return a response object because a response
object has all the information necessary to formulate an actual HTTP 
response. this object is then converted to text by the WSGI server which
called Pyramid, and it is sent back to the requestinng browser. 
To return a response, each view callable creates an instance of the 
Response class. In the hello_world function, a string is passed as the
body to the response.  
"""
