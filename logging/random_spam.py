import logging
import random


def generate_random_message():

    epithets = ['popular', 'legendary', 'eccentric',
                'ambitious', 'overwhelming', 'badass']

    people = ['Boka', 'Joka', 'Erik',
              'Edik', 'Yamma', 'Natalya']

    return (f'{epithets[random.randint(0, 5)]} ' +
            f'{people[random.randint(0, 5)]}')


def random_logging_mode_message(msg):

    logging_modes = ['debug', 'info', 'warning', 'error', 'critical']

    return eval(f'logging.{logging_modes[random.randint(0, 4)]}(\'{msg}\')')


if __name__ == '__main__':

    log_fmt = (u'%(filename)s[LINE:%(lineno)d]#\n' +
               u'%(levelname)-8s\n' +
               u'[%(asctime)s]\n' +
               u'%(message)s\n')

    logging.basicConfig(format=log_fmt, level=logging.DEBUG,
                        filename='logs.txt', filemode='a')

    for _ in range(100):
        random_logging_mode_message(generate_random_message())



