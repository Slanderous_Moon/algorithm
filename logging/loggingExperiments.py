import logging

log_fmt = (u'%(filename)s[LINE:%(lineno)d]#\n'+
           u'%(levelname)-8s\n'+
           u'[%(asctime)s]\n'+
           u'%(message)s\n')

logging.basicConfig(format=log_fmt, level=logging.DEBUG)

print(logging.debug(u'This is a debug message'),
      logging.info(u'This is an info message'),
      logging.warning(u'This is warning'),
      logging.error(u'This is an error message'),
      logging.critical(u'FATAL!!!'))
