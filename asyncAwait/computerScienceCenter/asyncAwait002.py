# Реализация при помощи конечного автомата

import time
import random
from enum import Enum, auto
import heapq                    # Очередь с приоритетом


def random_delay():
    return random.random() * 5


def random_countdown():
    return random.randrange(5)


def rockets():
    N = 10_000
    return (
        (random_delay(), random_countdown())
        for _ in range(N)
    )


class RocketState(Enum):
    WAITING = auto()
    COUNTING = auto()
    LAUNCHING = auto()


class RocketOperation(Enum):
    WAIT = auto()
    STOP = auto()


class RocketLaunch:
    def __init__(self, delay, countdown):
        self._state = RocketState.WAITING
        self._delay = delay
        self._countdown = countdown

    def step(self):
        if self._state is RocketState.WAITING:  # Enum сравниваются через is
            self._state = RocketState.COUNTING
            return RocketOperation.WAIT, self._delay
        if self._state is RocketState.COUNTING:
            if self._countdown == 0:
                self._state = RocketState.LAUNCHING
            else:
                print(f'{self._countdown}...')
                self._countdown -= 1
                return RocketOperation.WAIT, 1
        if self._state is RocketState.LAUNCHING:
            print("Rocket Launched!")
            return RocketOperation.STOP, None

        assert False, self._state


def now():
    return time.time()


def run_finite_state_machine(rockets):
    start = now()
    work = [
        (start, ident, RocketLaunch(delay, countdown))
        for ident, (delay, countdown) in enumerate(rockets)
    ]
    while work:
        step_at, ident, launch = heapq.heappop(work)
        wait = step_at - now()
        if wait > 0:
            time.sleep(wait)
        operation, arg = launch.step()
        if operation is RocketOperation.WAIT:
            step_at = now() + arg
            heapq.heappush(work, (step_at, ident, launch))
        else:
            assert operation is RocketOperation.STOP


if __name__ == '__main__':

    run_finite_state_machine(rockets())

