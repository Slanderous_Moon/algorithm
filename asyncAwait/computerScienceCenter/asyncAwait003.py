# Неявный конечный автомат вынесен в функцию launch_rocket

import time
import random
from enum import Enum, auto
import heapq                    # Очередь с приоритетом


def random_delay():
    return random.random() * 5


def random_countdown():
    return random.randrange(5)


def sleep(delay):
    yield RocketOperation.WAIT, delay


def launch_rocket(delay, countdown):
    # block WAITING
    yield from sleep(delay)
    # block COUNTING
    for i in reversed(range(countdown)):
        print(f'{i + 1}...')
        yield from sleep(1)
    # block LAUNCHING
    print("Rocket launched!")


def rockets():
    N = 10_000
    return (
        (random_delay(), random_countdown())
        for _ in range(N)
    )


class RocketOperation(Enum):
    WAIT = auto()
    STOP = auto()


def now():
    return time.time()


def run_finite_state_machine(rockets):

    start = now()
    work = [
        (start, ident, launch_rocket(delay, countdown))
        for ident, (delay, countdown) in enumerate(rockets)
    ]

    while work:

        step_at, ident, launch = heapq.heappop(work)
        wait = step_at - now()
        if wait > 0:
            time.sleep(wait)

        try:
            operation, arg = launch.send(None)
        except StopIteration:
            continue

        if operation is RocketOperation.WAIT:
            step_at = now() + arg
            heapq.heappush(work, (step_at, ident, launch))
        else:
            assert False, operation


if __name__ == '__main__':

    run_finite_state_machine(rockets())