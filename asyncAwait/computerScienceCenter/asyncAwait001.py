# Эмуляция запуска 10_000 ракет при помощи потоков

import time
import random
from threading import Thread    # Модуль для создания потоков


def random_delay():
    return random.random() * 5


def random_countdown():
    return random.randrange(5)


def rockets():
    N = 10_000
    return (
        (random_delay(), random_countdown())
        for _ in range(N)
    )


def launch_rocket(delay, countdown):
    '''Запускает ракету спустя определённое время ожидания'''
    time.sleep(delay)
    for i in reversed(range(countdown)):
        print(f'{i+1}...')
        time.sleep(1)
    print('Rocked launched!')


def run_threads(rockets):
    threads = (
        Thread(target=launch_rocket, args=(delay, countdown))
        for delay, countdown in rockets
    )
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()


if __name__ == '__main__':

    #launch_rocket(2, 3)

    #for delay, countdown in rockets():
    #    launch_rocket(delay, countdown)

    run_threads(rockets())