from flask import Flask
from flask import request
from flask import make_response
from flask import redirect

app = Flask(__name__)


@app.route('/')
def index():
    return '<h1>Hello World!</h1>'


@app.route('/user_agent')
def user_agent():
    user_agent = request.headers.get('User-Agent')
    return f'<p>Your browser is {user_agent}</p>'


@app.route('/user/<name>')
def user(name):
    return f'<h1>Hello, {name}!</h1>'


@app.route('/cookie_monster/')
def cookie_monster():
    response = make_response('<h1>This document carries a cookie!</h1>')
    response.set_cookie('answer', '42')
    return response


@app.route('/let_me_google_it_for_you/<word>')
def let_me_google_it_for_you(word):
    return redirect(f'google.com/search?q={word}')


if __name__ == '__main__':
    app.run(debug=True, port=4747)