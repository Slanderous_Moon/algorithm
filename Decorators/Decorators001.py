DEBUG = False

# Добавление к функции нового поведения
def trace(func):
    '''Traces called fuctions.'''
    if not DEBUG:
        return func

    def inner(*args, **kwargs):
        call = ", ".join(
            [str(arg) for arg in args] +
            [f'{key}={value}' for key, value in kwargs]
        )
        print(f'{func.__name__}({call}) = ...')
        ret = func(*args, **kwargs)
        print(f'{func.__name__}({call}) = {ret}')
        return ret

    inner.__name__ = func.__name__
    inner.__doc__ = func.__doc__
    inner.__module__ = func.__module__

    return inner


# Сломанная функция, которая не ищет максимум
@trace
def max(*args):
    '''Finds te latest argument. BROKEN'''
    ret = 0
    for x in args:
        ret = ret if x < ret else x
    return ret


if __name__ == '__main__':

    print(max(-10, -1, -3))