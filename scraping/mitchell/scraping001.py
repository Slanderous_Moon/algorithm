import sys

from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup


def get_title(url):
    try:
        html = urlopen(url)
    except HTTPError as e:
        print(e, file=sys.stderr)
        return None

    try:
        bsObj = BeautifulSoup(html.read(), "lxml")
        title = bsObj.body.h1
    except AttributeError as e:
        print(e, file=sys.stderr)
        return None

    return title


if __name__ == '__main__':

    url = ("http://vkirove.ru/news/2019/07/31/" +
           "na_ostanovke_u_zavoda_lepse_vysushili_vechnuyu_luzhu.html")

    title = get_title(url)
    if title is None:
        print("Title could not be found")
    else:
        print(title)

    html = urlopen(url)
    print(html.read())
    bsObj = BeautifulSoup(html.read(), 'lxml')
    print(dir(bsObj))
    print(bsObj.__dict__)

