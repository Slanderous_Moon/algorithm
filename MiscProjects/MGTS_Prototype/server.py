import json
import time
import sys
import os.path

import pymongo
import requests

import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.options

from tornado.options import define, options

MONGO = ''
FILE_NAME = 'lead_xml_body_example.xml'
FORM_PATH = os.path.join(os.path.dirname(__file__), 'examples', FILE_NAME)
LEAD_REDIRECTION_URL = 'https://test-extstat.mgts.ru/pmsb/elka_mobile'
LEAD_HEADERS = {
    'Content-Type': 'text/xml',
    'SOAPAction': 'http://schemas.mgts.ru/ELKA/v001/IElkaMobileWcf/SetOrderToConnect'
}



define(
    'port',
    default=8000,
    help='Run on the given port',
    type=int
)


class Application(tornado.web.Application):
    def __init__(self):

        handlers = [
            (r'/stat', StatHandler),
            (r'/lead', LeadHandler)
        ]
        settings = {'debug': True}

        db_client = pymongo.MongoClient('localhost', 27017)
        self.database = db_client.prototype_database

        tornado.web.Application.__init__(self, handlers, **settings)


class JsonFilesHandler(tornado.web.RequestHandler):
    """
    Metaclass that is inherited by StatHandler and LeadHandler.
    """
    handler_collection = ''

    def _initialize(self):
        self.decoded_data = None

    def prepare(self):

        try:
            self.decoded_data = json.loads(self.request.body)
        except json.decoder.JSONDecodeError as e:
            error_time = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
            error_msg = f'Not JSON file, time: {error_time}'
            print(e, error_msg, sep='\n',)

    def post(self):
        # create/open handler collection
        coll = self.application.database[self.handler_collection]
        data_to_insert = self.decoded_data
        if data_to_insert:
            insertion_time = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
            data_to_insert['date_added'] = insertion_time
            coll.insert_one(data_to_insert)
        else:
            print('Insertion failed.')


class StatHandler(JsonFilesHandler):
    """
    Saves user_statistics in mongoDB stats_collection
    """
    handler_collection = 'stats_collection'


class LeadHandler(JsonFilesHandler):
    xml_form = FORM_PATH
    headers = LEAD_HEADERS
    redirection_url = LEAD_REDIRECTION_URL
    handler_collection = 'lead_collection'
    form_insertions = {
        'address': '',
        'comment': '',
        'contact_info': '',
        'contact_phone': '',
        'email': '',
        'friend_phone': '',
        'phone': ''
    }

    def _initialize(self):
        self.filled_form = None

    def fill_form_with_decoded_data(self):

        # Fill lead_xml_body_example.xml with decoded data
        try:
            for key in self.decoded_data.keys():
                if key in self.form_insertions:
                    self.form_insertions[key] = self.decoded_data[key]
        except AttributeError as e:
            error_time = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
            error_msg = f"Can't find decoded data: {error_time}"
            print(e, error_msg, sep='\n')

        with open(self.xml_form) as file_obj:
            form = file_obj.read()
        self.filled_form = form.format(**self.form_insertions)

    def post(self):
        super().post()
        self.fill_form_with_decoded_data()
        # send to required data to server
        if self.filled_form and self.decoded_data:
            requests.post(
                url=self.redirection_url,
                data=self.filled_form,
                headers=self.headers
            )


if __name__ == '__main__':
    tornado.options.parse_command_line()
    app = Application()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

