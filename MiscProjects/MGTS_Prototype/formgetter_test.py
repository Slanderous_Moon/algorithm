import os.path
import lxml
from lxml import etree
import urllib
import urllib.parse


FILE_NAME = 'lead_xml_body_example.xml'
PATH = os.path.join(os.path.dirname(__file__), 'examples', FILE_NAME)


def get_form(file_name=FILE_NAME):
    path = os.path.join(os.path.dirname(__file__), file_name)
    form = open(path, 'r')
    return form

"""
def convert_lead_body_form(form, *,
                 address=False,
                 comment=False,
                 contact_info=False,
                 contact_phone=False,
                 email=False,
                 friend_phone=False,
                 phone=False,
                 # resource=False
                           ):
    form_str = ''
    for string in form:

        if address:
            if string.startswith('')


        form_str += string
"""

def parse_xml(xml_file):

    with open(xml_file) as file_obj:
        xml = file_obj.read()

    root = etree.fromstring(xml)

    for appt in root.getchildren():
        for elem in appt.getchildren():
            if not elem.text:
                text = "None"
            else:
                text = elem.text

            print(elem.tag + " => " + text)


def inserter(xml_file):
    with open(xml_file) as file_obj:
        xml = file_obj.read()

    print(type(xml))
    print(repr(xml))

    print('\n'*3)

    insertions = {'contact_phone': '',
                  'address': '',
                  'comment': '',
                  'contact_info': '',
                  'email': '',
                  'friend_phone': '',
                  'phone': ''}

    xml = xml.format(**insertions)
    print(xml)


if __name__ == '__main__':
    parse_xml(PATH)
    inserter(PATH)
    with open(PATH) as file_obj:
        xml = file_obj.read()
    print(urllib.parse.urlencode({'xml': xml}))

