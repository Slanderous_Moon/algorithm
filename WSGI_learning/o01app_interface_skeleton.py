"""
WSGI app intrface is implemented as a callable object: function, method,
class or an instance with an object.__call__() method. That callable must:
1) accept two positional parameters:
    * A dictionary containing CGI like variables
    * a callback function that will be used by the application to send
        HTTP status code/message and HTTP headers to te server.
2) return the respose body to the server as strings wrapped in an iterable.
"""


# the app skeleton:
def application(
        # Accepts two arguments:
        # environ points to a dictionary containing CGI like environment
        # variables which is populated by the server for each received
        # request from the client
        environ,
        # start_response - callback funсtion supplied by the server
        # which takes the HTTP status and headers as arguments
        start_response):

    # Build the response body possibly
    # using the supplied environ dictionary

    response_body = f'Request method: {environ["REQUEST_METHOD"]}'

    # HTTP response code and message
    status = '200 OK'

    # HTTP headers expected by the client
    # They must be wrapped as a list of tupled pairs:
    # [(Header name, Header value)]
    response_headers = [
        ('Content-Type', 'text/plain'),
        ('Content-Lengts', str(len(response_body)))
    ]

    # Send them to the server using the supplied function
    start_response(status, response_headers)

    # return the response body. Notice it is wrapped
    # in a list although it could be any iterable
    return [response_body]

