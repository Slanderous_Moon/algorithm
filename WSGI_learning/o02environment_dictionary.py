#! /usr/bin/env python

# Python' bundled WSGI server
from wsgiref.simple_server import make_server

def application(environ, start_response):

    # sorting and stringifying the environment key, value pairs
    response_body = [
        f'{key, value}' for key, value in sorted(environ(items))
    ]