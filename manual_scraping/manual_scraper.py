import datetime
import sys

from urllib.request import urlopen
from bs4 import BeautifulSoup
from abc import ABC, abstractmethod

import pprint

PYTHON_VERSION = sys.version_info
if PYTHON_VERSION < (3, 0):
    import urlparse
else:
    import urllib.parse as urlparse


class ManualScraper(ABC):
    """
    Abstract class of manual page scraper. Redefine all @abstractmethod-s
    to use.
    """

    def __init__(self, url):
        self.url = url
        self.domain = self._extract_domain()
        self.normalized_url = self._normalize_url()
        self.html = urlopen(url)
        self.bs_object = BeautifulSoup(self.html.read())
        self.meta = self._extract_page_meta()
        # todo: ask somebody, how do we count views
        # self.views = self._extract_views_number()

    def make_document_for_mongo(self):
        """
        Makes document for mongo that emulate those, that are made
        during usual scraping process via scrapy.
        :return: dict
        """
        document = dict()
        document['_id'] = self.normalized_url
        document['website'] = self.website
        # document['views'] = self.views
        document['url'] = self.url
        document['title'] = self.meta['title']
        document['meta'] = self.meta
        return document

    @property
    def website(self):
        """
        Abstract attribute, should be implemented in inherited classes.
        Stipulates website that should be scrapped by the scrapper.
        :exception: raises NotImplementedError
        """
        raise NotImplementedError

    def _normalize_url(self):

        url = self.url
        url = url.replace('//m.', '//').replace('//M.', '//').lower()
        url = url.replace('https://', '//').replace('http://', '//')
        return url

    def _extract_page_meta(self):
        """
        Tries to parse all required meta from BeautifulSoup object.
        :return: dict with extracted meta
        """
        meta = dict()
        meta['meta_info'] = self._extract_meta_info()
        meta['title'] = (meta['meta_info'].get('og_title', None) or
                         self._extract_title())
        meta['publication_date'] = self._extract_publication_date()
        meta['image_url'] = (meta['meta_info'].get('og_image', None) or
                             self._extract_image_url())
        meta['description'] = (meta['meta_info'].get('og_description', None) or
                               self._extract_description())
        meta['text_len'] = self._extract_text_len()
        meta['canonical_url'] = self.normalized_url
        return meta

    def _extract_domain(self):
        scheme, netloc, path, params, query, fragment = urlparse.urlparse(
            self.url
        )
        return netloc

    @abstractmethod
    def _extract_title(self):
        pass

    @abstractmethod
    def _extract_publication_date(self):
        pass

    @abstractmethod
    def _extract_image_url(self):
        pass

    @abstractmethod
    def _extract_description(self):
        pass

    @abstractmethod
    def _extract_text_len(self):
        pass

    @abstractmethod
    def _extract_meta_info(self):
        pass

    @abstractmethod
    def _extract_views_number(self):
        pass


class EchoekbManualScraper(ManualScraper):

    website = 'echoekb.ru'
    _title_block_search_settings = ("div", {"class": "news_title"})
    _author_search_settings = ("span", {"class": "block_blog_fio"})
    _image_search_settings = ("span", {"class": "block_blog_pic"})
    _text_search_settings = ("div", {"class": "blog_text"})

    def _extract_page_meta(self):
        meta = super()._extract_page_meta()
        meta['author'] = self._extract_author()
        meta['author_occ'] = self._extract_author_occ()
        meta['category'] = self._extract_category()

        meta['title'] = '–'.join(meta['title'].split('–')[:-1]).strip()
        if not meta['image_url'] or meta['image_url'].endswith('logo-fb.png'):
            meta['image_url'] = self._extract_image_url()

        return meta

    def _extract_meta_info(self):
        """
        Extracts all "meta" elements from page.
        :return: meta_info dict or None
        """
        meta_info = dict()
        for meta_element in self.bs_object.findAll('meta'):
            if meta_element and meta_element.get('content'):
                if meta_element.get('name'):
                    meta_info[meta_element['name']] = meta_element['content']
                elif meta_element.get('property'):
                    field_name = meta_element['property'].replace(':', '_')
                    meta_info[field_name] = meta_element['content']
        return meta_info

    def _extract_author(self):
        """
        Tries to extract author name via css search.
        :return: author name string or None
        """
        author_block = self.bs_object.find(*self._author_search_settings)
        author_name_block = author_block.strong
        return author_name_block.get_text() if author_name_block else None

    def _extract_author_occ(self):
        """
        Tries to extract author position via css search.
        :return: author position string or None
        """
        author_block = self.bs_object.find(*self._author_search_settings)
        author_occ_block = author_block.span
        return author_occ_block.get_text() if author_occ_block else None

    def _extract_title(self):
        """
        Tries to extract title via css selector.
        :return: title string or None
        """
        title_block = self.bs_object.find(*self._title_block_search_settings)
        return title_block.get_text() if title_block else None

    def _extract_publication_date(self):
        """
        Tries to parse publication date from url.
        :return: datetime obj or None
        """
        url = self.url
        url = url.replace('http://', '').replace('https://', '')
        try:
            domain, category, year, month, day, _ = url.split('/', 5)
        except ValueError:  # Irrelevant page
            return None
        timetuple = tuple(map(int, (year, month, day)))
        return datetime.datetime(*timetuple)

    def _extract_category(self):
        """
        Tries to parse category of the page from url.
        :return: category string or None
        """
        url = self.url
        url = url.replace('http://', '').replace('https://', '')
        try:
            domain, category, _ = url.split('/', 2)
        except ValueError:  # Irrelevant page
            return None
        return category

    def _extract_image_url(self):
        """
        Tries to extract image url via css search.
        :return: image url string or None
        """
        image_block = self.bs_object.find(*self._image_search_settings).img
        if image_block and image_block.get("src"):
            image_url = image_block["src"]
            if not (image_url.startswith('http') or
                    image_url.startswith('//')):  # not absolute
                image_url = '//' + self.domain + image_url
            return image_url
        else:
            return None

    def _extract_description(self):
        """
        Tries to extract first paragraph as description.
        :return: description string or None
        """
        text_block = self.bs_object.find(*self._text_search_settings)
        if text_block:
            text = text_block.get_text().strip()
            description = text[:text.find('\n')]
            return description
        else:
            return None

    def _extract_text_len(self):
        """
        Tries to extracts text block and counts it's length.
        :return: length of text or None
        """
        text_block = self.bs_object.find(*self._text_search_settings)
        if text_block:
            text = text_block.get_text()
            return len(text)
        else:
            return None

    def _extract_views_number(self):
        """
        Method under construction.
        """
        # todo: ask somebody, how should we count views.
        return None


if __name__ == '__main__':
    page_scraper = EchoekbManualScraper('https://www.echoekb.ru/blogs/2019/11/26/6350/0/')
    pprint.pprint(page_scraper.meta)
    pprint.pprint(page_scraper.make_document_for_mongo())