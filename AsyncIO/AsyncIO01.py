"""
AsyncIO - это независимая от конкретного языка парадигма написания сопрограмм,
которая получила поддержку в Python начиная с версии 3.4

Сопрограммы - специализированные функции-генераторы - это основа async IO в
языке Python.

Параллельность заключается в выполнении нескольких задач за один промежуток
времени. Мультипроцессинг - это практическое воплощение концепции
параллельности, он подразумеват распределение задач по ядрам (CPU) процессора.
Мультипроцессинг отлично подходит для вычислительных задач (CPU-bound).

Concurrency (совместное выполнение) - несколько более широкий термин, чем
параллельность. Данный подход предполагает возможность выполнять задачи в
дублирующей, перекрывающейся манере, но не подразумевает только лишь
параллельности.

Многопоточность - это модель совместного выполнения, при которой при выполнении
задачи несколько потоков выполняются по очереди. Многопоточность больше
подходит для заданий, для которых программе приходится больше ждать, чем
вычислять. (IO-bound)

AsyncIO - это концепция однопоточного и однопроцессного дизайна, в данном
случае используется так называемая совместная мультизадачность.
Асинхронность означает следующее:
* Асинхронные процессы могут "встать на паузу" в ожидании своего конечного
    результата, позволяя в такие моменты запускаться другим таким же процессам.
* Асинхронный код, несмотря на механизм выполнения, облегчает параллельное
    выполнение. Тут происходит как бы "имитация" параллельности.
"""

# Пример асинхронности из жизни:
"""
Гроссмейстер играет с любителями на шахматной выставке. Он может выбрать два
способа проведения данной выставки - синхронный и асинхронный. Предположим,
что у гроссмейстера 24 оппонента, гроссмейстер тратит 5 секунд на ход, а его
оппоненты - по 55 секунд. Игры обычно заканчиваются за 30 ходов с каждой 
стороны.
При синхронном подходе гроссмейстер играет только одну игру в один момент
времени, пока не сыграет со всеми. Каждая игра занимает (55 + 5) * 30 = 1800
секунд, или 30 минут. Вся выставка продлится 12 часов.

Асинхронная версия: гроссмейстер ходит между столами, делая по одному ходу 
за каждым столом. Затем гроссмейстер покидает стол, и даёт оппоненту время
обдумать свой следующий ход. Гроссмейстер сделает один ход за каждым из 24 
столов за 24 * 5 = 120 секунд, или за 2 минуты. Вся выставка продлится 
120 * 30 = 3600 секунд, или всего час.
"""

"""
Таким образом, кооперативный мультитаскинг - это всего лишь витиеватый способ 
сказать, что эвент-луп программы общается со многими задачами, позволяя каждой
отработать за оптимальное время.

Сопрограмма - это функция, которая умеет отложить своё выполнение до достижения
строчки return, и она может непрямым образом отдать контроль другой сопрограмме
на какое-то время. 
"""

# hello_world.py:

import asyncio
import time


# synchronous version
def count():
    print("One")
    time.sleep(1)
    print("Two")


def main():
    for _ in range(3):
        count()


# asynchronous version
async def async_count():
    print("One")
    await asyncio.sleep(1)
    print("Two")


async def async_main():
    await asyncio.gather(async_count(), async_count(), async_count())


if __name__ == '__main__':
    s = time.perf_counter()
    main()
    elapsed = time.perf_counter() - s
    print(f"sync part of the {__file__} executed in {elapsed:0.2f} seconds.")
    s = time.perf_counter()
    asyncio.run(async_main())
    elapsed = time.perf_counter() - s
    print(f"async part of the {__file__} executed in {elapsed:0.2f} seconds.")


"""
В данном случае time.sleep() может имитировать запуск вызов любой блокирующей
функции, потребляющей время, в то время как asyncio.sleep() используется для 
имитации любого неблокирующего вызова.
"""