# Использование очередей.

"""
В asyncio реализованы несколько классов очередей, которые схожи с
одноимёнными классами из модуля queue.

Эта структура так же может работать с async IO - ряд не связанных друг
с другом сопрограмм, которые могут добавлять какие-то позиции в очередь.
Каждая сопрограмма может добавлять какие-то позиции несистематично, в
случайный и заранее не извесный момент времени. В то же время группа
потребителей данных позиций может вытягивать позиции из очереди как только
они появляются, "жадным" образом, не ожидая никакого сигнала.

В данном дизайне какое-либо связывание в цепочку производителя и потребителя
отсутствует. Потребители не знают заранее ни количества производителей,
ни даже совокупного количества позиций, которые будут добавлены в очередь.
У каждого отдельного потребителя и производителя добавление в очередь и
вытягивание из очереди позиции занимает какое-то своё, произвольное
количество времени. Очередь в данном случае играет роль "конвейера" (в
оригинале thoughtput - пропускная способность), который умеет общаться с
производителями и потребителями, в то время как они между собой напрямую
не общаются.

Синхронная реализация подобной схемы весьма уныла - группа блокирующих
производителей систематически добаляют элементы в очередь, один производитель
за раз. Только после того, как все производители закончили добавлять
элементы, из очереди можно вытаскивать элементы - по одному потребителю за
раз, работая по очереди с каждым элементом. В таком дизайне куча задержек.
Элементы должны терпеливо ожидать своей участи в очереди, вместо того, чтобы
быть немедленно обработанными.

Основная сложность реализации асинхронной версии состоит в том, чтобы
отправить сигнал потребителям, что очередь закончилась. В противном случае
конструкция await q.get() будет бесконечно висеть, поскольку очередь
полностью обработана, но потребители об этом не подозревают.
"""

# Реализация скрипта:

import asyncio
import itertools as it
import os
import random
import time


async def make_item(size: int = 5) -> str:
    return os.urandom(size).hex()


async def rand_sleep(a: int = 1,
                     b: int = 5,
                     caller=None) -> None:
    i = random.randint(0, 10)
    if caller:
        print(f"{caller} sleeping for {i} seconds.")
    await asyncio.sleep(i)


async def produce(name: int,
                  q: asyncio.Queue) -> None:
    n = random.randint(0, 10)
    for _ in it.repeat(None, n):  # Synchronous loop for each single producer
        await rand_sleep(caller=f"Producer {name}")
        i = await make_item()
        t = time.perf_counter()
        await q.put((i, t))
        print(f"Producer {name} added <{i}> to queue.")


async def consume(name: int,
                  q: asyncio.Queue) -> None:
    while True:
        await rand_sleep(caller=f"Consumer {name}")
        i, t = await q.get()
        now = time.perf_counter()
        print(f"Consumer {name} got element <{i}>"
              f" in {now - t:0.5f} seconds.")
        q.task_done()


async def main(nprod: int,
               ncon: int):

    q = asyncio.Queue()
    producers = [asyncio.create_task(produce(n, q)) for n in range(nprod)]
    consumers = [asyncio.create_task(consume(n, q)) for n in range(ncon)]
    await asyncio.gather(*producers)
    await q.join()  # Implicitly awaits consumers, too
    for c in consumers:
        c.cancel()


if __name__ == '__main__':
    import argparse
    random.seed(444)
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--nprod", type=int, default=5)
    parser.add_argument("-c", "--ncon", type=int, default=10)
    ns = parser.parse_args()
    start = time.perf_counter()
    asyncio.run(main(**ns.__dict__))
    elapsed = time.perf_counter() - start
    print(f"Program completed in {elapsed:0.5f} seconds.")
