# rand.py

import asyncio
import random

# ANSI colours
c = (
    "\033[0m",      # End of color
    "\033[36m",     # Cyan
    "\033[91m",     # Red
    "\033[35m"      # Magenta
)


async def make_random(
        idx: int,
        threshold: int = 6
) -> int:
    print(c[idx + 1] + f"Initiated make_random({idx}).")
    i = random.randint(0, 10)
    while i <= threshold:
        print(c[idx + 1] + f"make_random({idx}) == {i} too low; retrying.")
        await asyncio.sleep(idx + 1)
        i = random.randint(0, 10)
    print(c[idx + 1] + f"---> Finished: make_random({idx}) == {i}" + c[0])
    return i


async def main():
    res = await asyncio.gather(*(make_random(i, 10 - i - 1) for i in range(3)))
    return res


if __name__ == '__main__':
    random.seed(random.randint(1, 444))
    r1, r2, r3 = asyncio.run(main())
    print()
    print(f'r1: {r1}, r2: {r2}, r3: {r3}')


# Объяснение:
"""
Дана сопрограмма make_random(), которая продолжает поизводить случайные
целые величины в диапазоне [0, 10] до тех пор пока одно из них не 
превысит определённый порог. Данный пример показывает, как множественные
вызовы сопрограмм не мешают друг другу ожидать своего завершения. 

В данном случае программа использует одну главную сопрограмму (make_random),
и запускает её параллельно трижды для трёх разных входных данных. Большая часть
программ строятся по такому шаблону - когда исползуются маленькие сопрограмы и
одна функция-обёртка для совместного запуска сопрограмм. main() в данном случае
используется для сбора заданий путём распределения центральной сопрограммы по
какому-то множеству значений.
"""