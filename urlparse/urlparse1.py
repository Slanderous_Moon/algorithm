'''
Модуль разбивает URL-строки на компоненты
'''

import urllib.parse as urlparse

# Функция принимает url-строку в качестве аргумента, возвращает
# кортеж из шести элементов - scheme, netloc, path, params, query,
# fragment
site = urlparse.urlparse('http://matbugat.ru/news/?id=25984&rrewrew=4544454')
print(site)

scheme, netloc, path, params, query, fragment = site
print(query)

# Парсинг строки запроса
query_parsed = urlparse.parse_qs(query)
print(query_parsed)
query_new_parsed = {'id': query_parsed['id']}

# Операция, обратная parse_qs
query = urlparse.urlencode(query_new_parsed, doseq=True)
print(query)

# Операция, обратная urlparse
url = urlparse.urlunparse((scheme, netloc, path, params, query, fragment))
print(url)