from array import array
import math


class Vector2d:

    # Атрибут класса для преобразования данного класса в последовательность
    # байтов и наоборот
    typecode = 'd'

    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)

    def __iter__(self):
        # Наличие даннного метода делает экземпляры класса итерируемыми,
        # так что можно делать присваивание по кортежу
        return (element for element in (self.x, self.y))

    def __repr__(self):
        # Синтаксис {!r} позволяет получать представление объектов,
        # возвращаемое функцией repr(). 
        class_name = type(self).__name__
        return "{}({!r}, {!r})".format(class_name, *self)

    def __str__(self):
        return str(tuple(self))

    def __bytes__(self):
        # Преобразование к строке байтов
        return (bytes([ord(self.typecode)]) +
                bytes(array(self.typecode, self)))

    def __eq__(self, other):
        return tuple(self) == tuple(other)

    def __abs__(self):
        return math.hypot(self.x, self.y)

    def __bool__(self):
        return bool(abs(self))

    @classmethod
    def frombytes(cls, octets):
        # конструктор экземпляра класса из строки байтов
        typecode = chr(octets[0])   # Считывае typecode из первого байта
        # Создание объекта memoryview из двоичной последовательности октетов
        # и приведение его к типу typecode:
        memv = memoryview(octets[1:]).cast(typecode)
        # распаковывание memoryview, получившийся в результате приведения типа
        # и получаем пару аргументов, необходимых констрктору
        return cls(*memv)

    def angle(self):
        # Вернуть угол в радианах
        return math.atan2(self.y, self.x)

    def __format__(self, format_spec=''):
        # служит для форматирования каждого компонента метода
        # при окончании спецификатора формата на 'p', возвращает
        # модуль вектора и угол в радианах
        if format_spec.endswith('p'):
            format_spec = format_spec[:-1]
            coordinates = (abs(self), self.angle())
            outer_format = '<{}, {}>'
        else:
            coordinates = self
            outer_format = '({}, {})'
        components = (format(c, format_spec) for c in coordinates)
        return outer_format.format(*components)


if __name__ == '__main__':
    v1 = Vector2d(3, 4)
    print(v1.x, v1.y)
    x, y = v1
    print(x, y)
    print(repr(v1))
    print(v1)
    v1_clone = eval(repr(v1))
    print(v1_clone)
    print(v1_clone == v1)
    print(bytes(v1))
    print(abs(v1))
    print(bool(v1), bool(Vector2d(0, 0)))
    b = Vector2d.frombytes(bytes(v1))
    print(b, (b == v1))
    print(format(Vector2d(1, 1), 'p'))
    print(format(Vector2d(1, 1), '.3ep'))
    print(format(Vector2d(1, 1), '0.5fp'))

# Лирическое оступление по форматированию при выводе
"""
Встроенная функция format() и метод str.format() делегируют форматирование
конкретному типу, вызывая его метод .__format__(format_spec). format_spec
в данном случае - это спецификатор формата, который являетс ялибо вторым
аргументом при вызове format(my_obj, format_spec), либо равен тому, что
находится после двоеточия в поле подстановки, обозначаемом скобками {} внутри
форматной строки при вызове str.format()
Например, {0.mass:5.3e} имеет две разных нотации, где 0.mass - имя поля 
подстановки field_name, а часть 5.3e после двоеточия - спецификатор формата.
Нотация, применяемая в спецификаторе формата, называется также миниязыком
спецификации формата. 
Для нескольких встроенных типов в миниязыке спецификации формата предусмотрены
специальные коды представления. Для типа int, к примеру, поддерживаются коды 
b и x, обозначающие соответственно основание 2 и 16, а для типа float - код
f для вывода значения с фиксированой точкой и % для вывода в виде процента.

Миниязык спецификации формата расшияемый, потому что каждый класс может 
интерпретировать аргумент format_spec как ему вздумается. Классы из 
модуля datetime пользуюьтся одними и теми же форматными кодами в функции 
strftime() и в своих методах __format__. Примеры:
"""

if __name__ == '__main__':
    from datetime import datetime
    now = datetime.now()
    print(format(now, '%H:%M:%S'))
    print(f'Now is {now:%I:%M %p}')

"""
Если в классе не реализован метод __format__, то используется метод, 
унаследованный от object, который возвращает значение str(my_object).
Однако если передать спецификатор формата классу без реализованного
__format__, будет возбуждено исключение TypeError.
"""