

class Vector2d:

    # Атрибут класса для преобразования данного класса в последовательность
    # байтов и наоборот
    typecode = 'd'

    def __init__(self, x, y):
        self.__x = float(x)
        self.__y = float(y)

    # x и y реализованы в виде неизменяемых свойств, досупных только для
    # чтения, чтобы поддерживать хешируемость.
    @property       # данный декоратор помечает метод чтения свойства
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y
    # После объявления данных методов self.x и self.y означает чтение
    # открытых свойств, а не закрытых атрибутов.

    def __iter__(self):
        # Наличие даннного метода делает экземпляры класса итерируемыми,
        # так что можно делать присваивание по кортежу
        return (element for element in (self.x, self.y))

    def __repr__(self):
        # Синтаксис {!r} позволяет получать представление объектов,
        # возвращаемое функцией repr().
        class_name = type(self).__name__
        return "{}({!r}, {!r})".format(class_name, *self)

    def __str__(self):
        return str(tuple(self))

    def __bytes__(self):
        # Преобразование к строке байтов
        return (bytes([ord(self.typecode)]) +
                bytes(array(self.typecode, self)))

    def __eq__(self, other):
        return tuple(self) == tuple(other)

    def __abs__(self):
        return math.hypot(self.x, self.y)

    def __bool__(self):
        return bool(abs(self))

    @classmethod
    def frombytes(cls, octets):
        # конструктор экземпляра класса из строки байтов
        typecode = chr(octets[0])   # Считывае typecode из первого байта
        # Создание объекта memoryview из двоичной последовательности октетов
        # и приведение его к типу typecode:
        memv = memoryview(octets[1:]).cast(typecode)
        # распаковывание memoryview, получившийся в результате приведения типа
        # и получаем пару аргументов, необходимых констрктору
        return cls(*memv)

    def angle(self):
        # Вернуть угол в радианах
        return math.atan2(self.y, self.x)

    def __format__(self, format_spec=''):
        # служит для форматирования каждого компонента метода
        # при окончании спецификатора формата на 'p', возвращает
        # модуль вектора и угол в радианах
        if format_spec.endswith('p'):
            format_spec = format_spec[:-1]
            coordinates = (abs(self), self.angle())
            outer_format = '<{}, {}>'
        else:
            coordinates = self
            outer_format = '({}, {})'
        components = (format(c, format_spec) for c in coordinates)
        return outer_format.format(*components)

    def __hash__(self):
        return hash(self.x) ^ hash(self.y)


"""
Объект называется хэшируемым, если имеет хэш-значение, которое не изменяется
на протяжении всего времени его жизни (у него должен быть метод __hash__()),
и допускает сравнение с другими объектами (у него должен быть метод
__eq__( ) ). Если в результате сравнения хэшируемых объектов оказывается,
что они равны, то и их хэш-значения должны быть равны.

Все атомарные неизменяемые типы (str, bytes, числовые типы) являются
хэшируемыми. Объект типа frozenset всегда хэшируемый, потому что его
элементы должны быть хэшируемыми по определению. Объект типа tuple является
хэшируемым только тогда, которые хэшируемы все его элементы.

Любой пользовательский тип является хэшируемым по определе¬
нию, потому что его хэш -значение равно id о и никакие два объекта
этого типа не равны. Если объект реализует метод _ eq _ , учитываю¬
щий внутреннее состояние, то он будет хэшируемым, только если все
атрибуты неизменяемые.
"""