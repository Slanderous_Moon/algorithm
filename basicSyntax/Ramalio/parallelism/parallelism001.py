# concurrent.futures

'''
Эффективное программирование сетевого ввода-вывода невозможно без параллелизма
из-за наличия сетевых задержек.
'''

# Для начала пример последовательного скрипта:
"""
Скрипт используется в качестве эталона для сравнения с другими скриптами.
"""

import os
import time
import sys

import requests     # Пакет отделен так как не входит в стандартную библиотеку.

POP20_CC = ('CN IN US ID BR PK NG BD RU JP ' +
            'MX PH VN ET EG DE IR TR CD FR').split(' ')

BASE_URL = 'http://flupy.org/data/flags'

DEST_DIR = './downloads/'


def save_flag(img, filename):
    path = os.path.join(DEST_DIR, filename)
    with open(path, 'wb') as fp:
        fp.write(img)


def get_flag(cc):   # Загрузка изображения и возврат двоичного содержимого
    url = f'{BASE_URL}/{cc.lower()}/{cc.lower()}.gif'
    resp = requests.get(url)
    return resp.content


def show(text):
    print(text, end=' ')
    sys.stdout.flush()


def download_many(cc_list):
    for cc in sorted(cc_list):
        image = get_flag(cc)
        show(cc)
        save_flag(image, cc.lower() + '.gif')

    return len(cc_list)


def main(download_many):
    t0 = time.time()
    count = download_many(POP20_CC)
    elapsed = time.time() - t0
    msg = f'\n{count} flags downloaded in {elapsed:.2f}s'
    print(msg)


if __name__ == '__main__':

    try:
        os.mkdir(DEST_DIR)
    except OSError as e:
        pass

    main(download_many)


