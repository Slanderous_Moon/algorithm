# Примеры из криптографии

import sys
import time

from concurrent import futures
from random import randrange


JOBS = 12
SIZE = 2**18

KEY = b"'Twas brillig, and the slithy toves\nDid gyre"
STATUS = '{} workers, elapsed time: {:.2f}s'


# Алгоритм шифрования RC4
def arcfour(key, in_bytes, loops=20):
    kbox = bytearray(256)  # create key box
    for i, car in enumerate(key):  # copy key and vector
        kbox[i] = car
    j = len(key)
    for i in range(j, 256):  # repeat until full
        kbox[i] = kbox[i-j]

    # Инициализация sbox
    sbox = bytearray(range(256))

    # Повторение цикла перемешивания box, как рекомендовано в CipherSaber-2
    # http://ciphersaber.gurus.com/faq.html#cs2
    j = 0
    for k in range(loops):
        for i in range(256):
            j = (j + sbox[i] + kbox[i]) % 256
            sbox[i], sbox[j] = sbox[j], sbox[i]

    # main loop
    i = 0
    j = 0
    out_bytes = bytearray()

    for car in in_bytes:
        i = (i + 1) % 256
        # shuffling sbox
        j = (j + sbox[i]) % 256
        sbox[i], sbox[j] = sbox[j], sbox[i]
        # calculating t
        t = (sbox[i] + sbox[j]) % 256
        k = sbox[t]
        car = car ^ k   # двоичный xor
        out_bytes.append(car)

    return out_bytes


def test():
    """Tests RC4 algorithm realisation"""
    clear = bytearray(b'1234567890' * 100000)
    t0 = time.time()
    cipher = arcfour(b'key', clear)
    print(f'elapsed time: {time.time() - t0}')
    result = arcfour(b'key', cipher)
    assert result == clear, '%r != %r' % (result, clear)
    print(f'elapsed time:: {time.time() - t0}')
    print('OK')


def arcfour_test(size, key):
    in_text = bytearray(randrange(256) for i in range(size))
    cypher_text = arcfour(key, in_text)
    out_text = arcfour(key, cypher_text)
    assert in_text == out_text, 'Failed arcfour_test'
    return size


def main(workers=None):
    if workers:
        workers = int(workers)
    t0 = time.time()
    with futures.ProcessPoolExecutor(workers) as executor:
        actual_workers = executor._max_workers
        to_do = []
        for i in range(JOBS, 0, -1):
            size = SIZE + int(SIZE / JOBS * (i - JOBS/2))
            job = executor.submit(arcfour_test, size, KEY)
            to_do.append(job)

        for future in futures.as_completed(to_do):
            res = future.result()
            print(f'{res/2**10:.1f} KB')

        print(STATUS.format(actual_workers, time.time() - t0))


if __name__ == '__main__':
    test()

    if len(sys.argv) == 2:
        workers = int(sys.argv[1])
    else:
        workers = None
    main(workers)