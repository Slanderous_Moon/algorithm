"""
Будущие объекты - компоненты внутреннего механизма пакетов concurrent.futures и
asyncio. В стандартной библиотеке есть два класса с именем Future:
concurrent.futures.Future и asyncio.Future. Служат эти классы для
представления неких отложенных вычислений, завершившихся или нет. В качестве
аналогов можно привести Deferred из Twisted или Future в Tornado.

Будущие объекты инкапсулируют ожидающие операции, так чо их можно помещать в
очереди, опрашивать состояние завершения и получать результаты (или исключения)
по доступности. Предполагается, что будущие объекты создаёт исключительно
используемая библиотека.

Оба класса Future имеют неблокирующий метод Future.done(), который показывает,
завершился ли вызываемый объект, связанный с экземпляром класса, и возвращая
булево значение. Клиент обычно не проверяет состояние сам, а просит, чтобы его
уведомили, потому у классов есть метод .add_done_callback() - если передать ему
вызываемый объект, то он будет вызван, когда будущий объект завершится, а в
качестве единственного аргумента будет передан сам этот будущий объект.
"""

import sys, time, os

import requests
from concurrent import futures


POP20_CC = ('CN IN US ID BR PK NG BD RU JP ' +
            'MX PH VN ET EG DE IR TR CD FR').split(' ')

BASE_URL = 'http://flupy.org/data/flags'

DEST_DIR = './downloads/'


def save_flag(img, filename):
    path = os.path.join(DEST_DIR, filename)
    with open(path, 'wb') as fp:
        fp.write(img)


def get_flag(cc):
    url = f'{BASE_URL}/{cc.lower()}/{cc.lower()}.gif'
    resp = requests.get(url)
    return resp.content


def show(text):
    print(text, end=' ')
    sys.stdout.flush()


def main(download_many):
    t0 = time.time()
    count = download_many(POP20_CC)
    elapsed = time.time() - t0
    msg = f'\n{count} flags downloaded in {elapsed:.2f}s'
    print(msg)


MAX_WORKERS = 20


def download_one(cc):
    image = get_flag(cc)
    show(cc)
    save_flag(image, cc.lower() + '.gif')
    return cc


def download_many(cc_list):
    """
    Переписана для использования функции concurrent.futures.as_completed.
    Вызов .map() заменён на два цикла: один  для создания и планирования
    будущих объектов, второй - для получения их результатов.
    """
    cc_list = cc_list[:5]
    with futures.ThreadPoolExecutor(max_workers=3) as executor:
        to_do = []
        for cc in sorted(cc_list):
            future = executor.submit(download_one, cc)
            to_do.append(future)
            msg = f'Scheduled for {cc}: {future}'
            print(msg)

        results = []
        for future in futures.as_completed(to_do):
            res = future.result()
            msg = f'{future} result: {res}'
            print(msg)
            results.append(res)

    return len(results)


if __name__ == '__main__':
    main(download_many)