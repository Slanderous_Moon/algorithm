# concurrent.futures

import sys, time, os

import requests
from concurrent import futures


POP20_CC = ('CN IN US ID BR PK NG BD RU JP ' +
            'MX PH VN ET EG DE IR TR CD FR').split(' ')

BASE_URL = 'http://flupy.org/data/flags'

DEST_DIR = './downloads/'


def save_flag(img, filename):
    path = os.path.join(DEST_DIR, filename)
    with open(path, 'wb') as fp:
        fp.write(img)


def get_flag(cc):
    url = f'{BASE_URL}/{cc.lower()}/{cc.lower()}.gif'
    resp = requests.get(url)
    return resp.content


def show(text):
    print(text, end=' ')
    sys.stdout.flush()


def main(download_many):
    t0 = time.time()
    count = download_many(POP20_CC)
    elapsed = time.time() - t0
    msg = f'\n{count} flags downloaded in {elapsed:.2f}s'
    print(msg)


"""
Основой пакета concurrent.futures являются классы ThreadPoolExecutor и 
ProcessPoolExecutor, которые реализуют интерфейс, позволяющий передавать
вызываемые объекты соответственно потокам или процессам. Оба класса управляют
пулом рабочих потоков или процессов и очередью подлежащих выполнению задач. 
"""

# Простейший способ параллельной загрузки:

MAX_WORKERS = 20    # 1.


def download_one(cc):   # 2.
    image = get_flag(cc)
    show(cc)
    save_flag(image, cc.lower() + '.gif')
    return cc


def download_many(cc_list):
    workers = min(MAX_WORKERS, len(cc_list))    # 3.
    with futures.ThreadPoolExecutor(workers) as executor:   # 4.
        res = executor.map(download_one, sorted(cc_list))   # 5.

    return len(list(res))   # 6.


if __name__ == '__main__':
    main(download_many)


"""
1. Установлено максимальное количество потоков в объекте ThreadPoolExecutor.
2. Функция, загружающая одно изображение, её будет использовать каждый поток.
3. Количество рабочих потоко - минимум из максимально допустимого числа потоков
    и фактического числа подлежащих обработке элементов, чтобы лишние потоки
    не были созданы.
4. Создаётся экземпляр ThreadPoolExecutor. Метод executor.__exit__ вызовет
    exector.shutdown(wait=True), который блокирует выполенеие программы до
    завершения всех потоков. 
5. Метод map похож на одноимённую встроенную функцию с тем исключением, что
    переданная функция download_one вызывается одновременно из нескольких 
    потоков. Метод возвращает генератор, который можно будет обойти для 
    получения значений, возвращённых каждой функцией. 
6. Количество полученных результатов возвращается для того, чтобы отследить
    возможное исключение, если оно было возбуждено в одном из потоков.
    В этом место оно возникнет, когда неявный вызов next() попытается 
    получить соответствующее исключение от итератора.
    
download_one в данном примере является как бы телом цикла for в функции
download_many из предыдущего примера. Это типичный рефакторинг при написании
параллельного кода: тело цикла преобразуется в функцию, которая будет 
запускаться параллельно. 
"""