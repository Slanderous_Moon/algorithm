# Возврат значений из сопрограммы

'''
Одна из причин существования конструкции yield from, помимо более
удобного возврата значений из сопрограмм - это возбуждение исключений
во вложенных сопрограммах.

averager3 возвращает результат в виде именованного кортежа,
содержащего количество усреднённых элементов count и среднее average.
'''
from functools import wraps
from collections import namedtuple

Result = namedtuple('Result', 'count average')


def coroutine(func):
    '''
    Decorator: primes 'func' by advancing to first 'yield'.
    '''
    @wraps(func)
    def primer(*args, **kwargs):
        gen = func(*args, **kwargs)
        next(gen)
        return gen
    return primer


@coroutine
def averager3():
    total = 0.0
    count = 0
    average = None
    while True:
        term = yield
        if term is None:
            break
        total += term
        count += 1
        average = total/count
    return Result(count, average)


if __name__ == '__main__':
    coro_avg4 = averager3()
    print(coro_avg4.send(10), coro_avg4.send(30), coro_avg4.send(3.5))
    try:
        print(coro_avg4.send(None))
    except StopIteration as e:
        print(e)

    coro_avg4 = averager3()
    for element in (10, 390, 2.5, None):
        try:
            coro_avg4.send(element)
        except StopIteration as e:
            print('StopIteration', e)
            result = e.value
            print('res = ', result)

'''
Необходимо обратить внимание на то, что значение выражения return 
передаётся вызывающей стороне в виде атрибута объекта-исключения StopIteration.
Это не совсем честно, но сохраняет существующее поведение объектов-генераторов:
возбуждение StopIteration по исчерпании.
Стоит отметить, что yield from автоматически перехватывает StopIteration
внутри себя и принимает возвращённое значение value.
'''

# yield from

'''
Когда генератор gen вызывает yield from subgen(), subgen перехватывает 
управление и начинает отдавать значения непосредственно функции, из которой
был вызван gen, вызывающая сторона напрямую управляет subgen. Тем временем 
gen остаётся блокированным в ожидании завершения subgen. 
'''

# yield from можно использовать вместо yield в цикле


def gen():
    for c in 'AB':
        yield c
    for i in range(1, 3):
        yield i


print(list(gen()))


# Аналог:
def gen():
    yield from 'AB'
    yield from range(1, 3)
print(list(gen()))


# Сцепление итерируемых объектов при помощи yield from

def chain(*iterables):
    for it in iterables:
        yield from it
s = 'ABC'
t = tuple(range(3))
print(list(chain(s, t)))


# Схема работы yield from

'''
Первое, что делает yield from x с объектом x, - вызов iter(x). 
x может быть произвольным итерируемм объектом.

Основное применение yield from - открытие двустороннего канала между внешней 
вызывающей программой и внутренним субгенератором, так чтобы значения можно
было отправлять и отдавать напрямую, а исключения возбуждать и обрабатывать без 
написания кучи кода в промежуточных сопрограммах. И потому в результате 
появляется такое поняие как делегирование сопрограмме.

Делегирующий генератор - генераторная функция, содержащая выражение 
yield from <iterable> 

субгенератор - генератор, полученный от итерируемого объекта <iterable> 
в выражении yield from. 

вызывающая сторона - клиентский код, который вызывает делегирующий генератор.
Можно использовать термин "клиент". 
'''

# Пример.
# Подсчёт средних роста и веса мальчиков и девочек гипотетического класса:

Result = namedtuple('Result', 'count average')

# Субгенератор
def averager():
    total = 0.0
    count = 0
    average = None
    while True:
        term = yield
        if term is None:
            break
        total += term
        count += 1
        average = total/count
    return Result(count, average)


# делегирующий генератор
def grouper(results, key):
    while True:
        results[key] = yield from averager()


# клиент/вызывающая сторона
def main(data):
    results = {}
    for key, values in data.items():
        group = grouper(results, key)
        next(group)
        for value in values:
            group.send(value)
        group.send(None) #
        # print(results)    # Отладочный код
    report(results)


# Вывод отчёта
def report(results):
    for key, result in sorted(results.items()):
        group, unit = key.split(';')
        print(f'{result.count :2} {group :5} averaging ' +
              f'{result.average :.2f}{unit}'
              )


data = {
    'girls;kg':
        [40.9, 38.5, 44.3, 42.2, 45.2, 41.7, 44.5, 38.0, 40.6, 44.5],
    'girls;m':
        [1.6, 1.51, 1.4, 1.3, 1.41, 1.39, 1.33, 1.46, 1.45, 1.43],
    'boys;kg':
        [39.0, 40.8, 43.2, 40.8, 43.1, 38.6, 41.4, 40.6, 36.3],
    'boys;m':
        [1.38, 1.5, 1.32, 1.25, 1.37, 1.48, 1.25, 1.49, 1.46],
}

if __name__ == '__main__':
    main(data)

'''
average - это субгенератор. Каждое значение, которое будет отправлено из main,
будет связано c переменной term данного субгенератора.
grouper - делегирующий генератор. На каждой итерации его цикла будет создан 
новый экземпляр averager, каждый является генератором, работающим как 
сопрограмма.
Значение, отправляемое генератору grouper, помещается выражением yield from 
в канал, открытый с объектом averager. grouper остаётся приосановленным, пока
averager потребляет отправленные клиентским кодом значения. Когда выполнение
averager завершится, возвращённое значение будет связано с results[key].
После этого будет создан очередной экземпляр averager для потребления 
последующих значений.

group.send(None) завершает работу одного объекта averager и запускает 
следующий. При отсутствии этой строчки сопрограмма averager() никогда не 
остановится, и results[key] ничего присвоено не будет.

В данном примере делегирующий генератор работает как канал. Все значения 
отправляются напрямую субгенератору, делегирующий ничего не получает. 
Делегирующих генераторов может быть сколько угодно, из них можно сформировать
конвейер, однако эта цепочка должна заканчиваться простым генератором,
в котором используется обычный yield, или произвольным итерируемым объектом.
любая подобная цепочка должна управляться клиентом, который вызывает next() 
или .send() для самого внешнего делегирующего генератора.
'''
