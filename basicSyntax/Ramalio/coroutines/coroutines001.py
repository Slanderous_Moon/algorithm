from inspect import getgeneratorstate
import random
from functools import wraps
import sys


# Сопрограммы
'''
Строка вида yield item порождает значение, которое получает сторона, вызвавшая
функцию next(). Кроме того, она уступает процессор, приостанавливая выполнение
генератора, чтобы вызывающая сторона могла продолжить работу до момента, когда
ей понадобится следующее значение от next().

Выглядит сопрограмма синтаксически как генератор - функция, в теле которой
встречается ключевое слово yield. В сопрограмме yield обычно находится в правой
части выражения присваивания (datum = yield) и может порождать или не порождать
значение - если после слова yield нет никакого выражения, генератор отдаёт
None.

Сопрограмма может получать данные от вызывающей стороны, если та вместо next()
воспользуется методом .send(datum).

Независимо от потока данных yield является средством управления потоком
выполнения, которое можно использовать для реализации невытесняющей
многозадачности: каждая сопрограмма уступает управление центральному
планировщику, чтобы тот мог активировать другие сопрограммы.
'''


def simple_coroutine(): # Определяется так же, как функция-генератор
    print('-> coroutine started')
    # yield используется в выражении присваивания
    # Если сопрограмма предназначена только для получения данных от клиента,
    # то yield поставляет None
    x = yield
    print('-> coroutine received: ', x)


my_coro = simple_coroutine()    # Вызов для получения объекта генератора
print(my_coro)
'''
Первой вызывается next, поскольку генератор ещё не начал свою работу, 
то есть ещё не приостановился, достигнув yield, потому послать данные ему
пока невозможно. 
'''
print(next(my_coro))
try:
    print(my_coro.send(42))
except StopIteration as e:
    '''
    Управление покидает тело сопрограммы, в результате чего генератор
    возбуждает исключение StopIteration.
    '''
    print('StopIteration', e)

# inspect.getgeneratorstate()
'''
Функция, которая помогает узнать, в каком именно из четырёх возможных
состояний находится сопрограмма:
'GEN_CREATED', 'GEN_RUNNING', 'GEN_SUSPENDED', 'GEN_CLOSED' 
'''


# Сопрограмма с несколькими yield:
def simple_coro2(a):
    print('-> Started: a = ', a)
    b = yield a
    print('-> Received: b = ', b)
    c = yield a + b
    print('-> Received: c = ', c)


if __name__ == '__main__':

    my_coro2 = simple_coro2(14)
    try:
        print(getgeneratorstate(my_coro2),  # Cтрокового представления нет
              next(my_coro2),
              getgeneratorstate(my_coro2),  # только для интерактивной оболочки
              my_coro2.send(28),
              getgeneratorstate(my_coro2),
              my_coro2.send(99),
              getgeneratorstate(my_coro2)
              )
    except StopIteration as e:
        print('StopIteration', e)







