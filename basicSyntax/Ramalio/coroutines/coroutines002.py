import random
from functools import wraps

# Вычисление накопительного среднего при помощи сопрограммы
'''
В бесконечном цикле сопрограмма будет принимать начения и порождать резутаты, 
пока вызывающая сторона их посылает. Завершится сопрограмма по вызову метода 
.close() или при уничтожении сборщиком мусора.

yielf используется, чтобы приостановить сопрограмму, отдать результат 
вызывающей стороне и впоследствии получить значение, посланное вызывающей
стороной, после чего выполнение бесконечного цикла продолжится.
'''


def averager():
    total = 0.0
    count = 0
    average = None
    while True:
        # Сперва сопрограмма поставит average, приостановится и возобновит
        # работу после получения term.
        term = yield average
        print('Received term: ', term)
        total += term
        count += 1
        average = total/count


'''
Преимущество сопрограмм для подобных вычисений в том, что total и count - это
обычные локальные переменные, ни замыкания, ни переменные экземпляра для 
запоминания контекста между вызовами не нужны. 
'''

# Пример работы:

if __name__ == '__main__':
    coro_avg = averager()
    next(coro_avg)
    for _ in range(10):
        print(coro_avg.send(random.randint(-10, 50)))

# Декораторы для инициализации сопрогамм

'''
Про инициализацию сопрограмм вызовом next() легко позабыть, потому для этих
целей сущесвует специальный декоратор.
'''


def coroutine(func):
    '''
    Decorator: primes 'func' by advancing to first 'yield'.
    '''
    @wraps(func)
    def primer(*args, **kwargs):
        gen = func(*args, **kwargs)
        next(gen)
        return gen
    return primer


@coroutine
def averager2():
    total = 0.0
    count = 0
    average = None
    while True:
        # Сперва сопрограмма поставит average, приостановится и возобновит
        # работу после получения term.
        term = yield average
        print('Received term: ', term)
        total += term
        count += 1
        average = total/count


if __name__ == '__main__':
    coro_avg2 = averager2()
    for _ in range(10):
        print(coro_avg.send(random.randint(-10, 110)))


'''
Некоторые фреймворки предлагают свои декораторы для работы с сопрограммами.
Не все предназначены для инициализации сопрограмм, некоторые предоставляют 
такие возможности, как, например, включение в цикл обработки событий. 
(torado.gen).

Такая конструкция, как yield from автоматически инициализирует вызываемую
с её помощью сопрограмму, а потому она не совместима с приведённым выше 
@coroutine. @asyncio.coroutine предназначен для работы с yield from, а потому
не инициализирует сопрограмму.
'''

# Завершение сопрограммы, обработка исключений.

'''
Необработанное исключение в сопрограмме распространяется в функцию, из 
которой был произведён вызов next или send, приведший к исключению.
'''

if __name__ == '__main__':
    coro_avg3 = averager2()
    print(coro_avg3.send(40), coro_avg3.send(50), sep='\n')
    # Отправка нечислового значения приводит к исключению
    try:
        print(coro_avg3.send('spam'))
    except TypeError as e:
        print(e)
    # Исключение не обработано сопрограммой, попытка активировать вызовет
    # исключение StopIteration
    try:
        print(coro_avg3.send(60))
    except StopIteration:
        print('Previous exception is not handled by the coroutine itself.' +
              'StopIteration')

    coro_avg3 = averager2()
    try:
        print(coro_avg3.send(StopIteration))
    except TypeError as e:
        print(e, 'StopIteration')
# Другие методы объектов-генераторов
'''
generator.throw(exc_type[, exc_value[, traceback]])

Приводит к тому, что yield, в котором генератор приостановился, возбуждает
указанное исключение. Если генератор обрабатывает подобное исключение, то
выполненеие продолжится до следующего yield, а оданное значение станет
значением вызова generator.throw. В противном случае исключение распространится
в контекст вызывающей стороны.

generator.close()

Выражение yield, в котором генератор приостановлен, возбуждает исключение 
GeneratorExit. Если это исключение не будет обработано генератором, или 
генератор возбудит исключение StopIteration, то вызывающая сторона не получит 
никакой ошибки. При получении исключения GeneratorExit, генератор не должен
отдавать значение иначе возникнет исключение RuntimeError.
Любое другое исключение распространится на контекст вызывающей стороны. 
'''


class DemoException(Exception):
    '''
    Exception type for demonstration.
    '''
    pass


def demo_exc_handling():
    print('-> coroutine started')
    while True:
        try:
            x = yield
        except DemoException:   # Специальная обработка DemoException
            print('*** DemoException handled. Continuing...')
        else:   # При остутствии исключение вывести значение
            print(f'-> Coroutine received: {x}')
    raise RuntimeError('This line should never run.')   # Не выполнится


if __name__ == '__main__':

    exc_coro = demo_exc_handling()
    next(exc_coro)
    print(exc_coro.send(11))
    exc_coro.throw(DemoException)
    print(exc_coro.send(12))
    try:
        exc_coro.throw(ZeroDivisionError)
    except ZeroDivisionError as e:
        print(e)

'''
Вне зависимости от способа завешения сопрограммы можно выполнять какой-то 
код очистки, для чего используется конструкция try/finally:
'''


def demo_finally():
    print('-> coroutine started')
    try:
        while True:
            try:
                x = yield
            except DemoException:
                print('*** DemoException handled. Continuing...')
            else:
                print(f'-> coroutine received: {x}')
    finally:
        print('-> coroutine ending')


