# Моделирование дискретных данных
'''
Моделирование дискретных событий - методика, предполагающая, что система
моделируется в виде хронологической последовательности событий. Часы при этом
сдвигаются сразу к модельному времени следующего моделируемого события.
Для моделирования DES (discrete event simulation) в Python есть пакет SimPy,
в котором каждый моделируемый процесс представлен одной сопрограммой.
'''


# Пример - моделирование работы таксопарка.
'''
Создается несколько экземпляров такси. Каждое такси совершает фиксированное 
количество поездок и возвращается в гараж. Такси выезжает из гаража и начинает 
«рыскать» - искать пассажира. Это продолжается, пока пассажир не сядет в такси,
в этот момент начинается поездка. Когда пассажир выходит, такси возвращается в 
режим поиска.
'''

import random, collections, queue, argparse, time


DEFAULT_NUMBER_OF_TAXIS = 3
DEFAULT_END_TIME = 180
SEARCH_DURATION = 5
TRIP_DURATION = 20
DEPARTURE_INTERVAL = 5

Event = collections.namedtuple('Event', 'time proc action')


# Begin Taxi Process
def taxi_process(ident, trips: "trips quantity", start_time=0):
    """Отдаёт модели событие при каждом изменении состояния"""
    time = yield Event(start_time, ident, 'leave garage')
    for i in range(trips):
        time = yield Event(time, ident, 'pick up passenger')
        time = yield Event(time, ident, 'drop off passenger')
    yield Event(time, ident, 'going home')
    # END TAXI_PROCESS


# BEGIN TAXI_SIMULATOR
class Simulator:
    def __init__(self, procs_map):
        self.events = queue.PriorityQueue()
        self.procs = dict(procs_map)

    def run(self, end_time):
        """Планирует и отображает события, пока не истечёт время"""
        # планируем первое событие для каждой машины
        for _, proc in sorted(self.procs.items()):
            first_event = next(proc)
            self.events.put(first_event)

        # Главный цикл моделирования
        sim_time = 0
        while sim_time < end_time:
            if self.events.empty():
                print('*** end of events ***')
                break

            current_event = self.events.get()
            sim_time, proc_id, previous_action = current_event
            print('taxi:', proc_id, proc_id * ' ', current_event)
            active_proc = self.procs[proc_id]
            next_time = sim_time + compute_duration(previous_action)
            try:
                next_event = active_proc.send(next_time)
            except StopIteration:
                del self.procs[proc_id]
            else:
                self.events.put(next_event)
        else:
            msg = '*** end of simulation time: {} events pending ***'
            print(msg.format(self.events.qsize()))
# END TAXI_SIMULATION


def compute_duration(previous_action):
    """Вычисление длительности действия с использованием
       экспоненциального распределения."""
    if previous_action in ['leave garage', 'drop off passenger']:
        # Новое состояние - поиск пассажира
        interval = SEARCH_DURATION
    elif previous_action == 'pick up passenger':
        # Новое состояние - поездка
        interval = TRIP_DURATION
    elif previous_action == 'going home':
        interval = 1
    else:
        raise ValueError(f'Unknown previous_action: {previous_action}')
    return int(random.expovariate(1/interval)) + 1


def main(end_time=DEFAULT_END_TIME, num_taxis=DEFAULT_NUMBER_OF_TAXIS,
         seed=None):
    """Инициализирует генератор случайных чисел, строит proc-объекты,
       запускает моделирование"""
    if seed is None:
        random.seed(seed)   # Для получения воспроизводимых результатов

    taxis = {i: taxi_process(i, (i+1)*2, i*DEPARTURE_INTERVAL)
             for i in range(num_taxis)}
    sim = Simulator(taxis)
    sim.run(end_time)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
                        description='Taxi fleet simulator.')
    parser.add_argument('-e', '--end-time', type=int,
                        default=DEFAULT_END_TIME,
                        help='simulation end time; default = ' +
                        f'{DEFAULT_END_TIME}')
    parser.add_argument('-t', '--taxis', type=int,
                        default=DEFAULT_NUMBER_OF_TAXIS,
                        help='number of taxis running; default = ' +
                        f'{DEFAULT_NUMBER_OF_TAXIS}')
    parser.add_argument('-s', '--seed', type=int, default=None,
                        help='random generator seed (for testing)')
    args = parser.parse_args()
    main(args.end_time, args.taxis, args.seed)
