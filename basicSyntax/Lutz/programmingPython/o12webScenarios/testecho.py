import sys

# Запускать при помощи командной строки из текущей папки
from launchmodes import QuietPortableLauncher

NUM_CLIENTS = 8


def start(cmdline):
    QuietPortableLauncher(cmdline, cmdline)()


start('echo-server.py')  # Запустить сервер локально если не запущен

args = ' '.join(sys.argv[1:])   # Передать имя сервера, если запущен удалённо

for i in range(NUM_CLIENTS):
    start(f'echo-client.py {args}')

