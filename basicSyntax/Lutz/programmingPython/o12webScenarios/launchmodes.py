r"""
Запускает программы Python с помощью механизмов командной строки и классов
схем запуска; автоматически вставляет “python” и/или путь к выполняемому файлу
интерпретатора в начало командной строки; некоторые из инструментов в этом
модуле предполагают, что выполняемый файл ‘python’ находится в системном пути
поиска (смотрите Launcher.py);

можно было бы использовать модуль subprocess, но он сам использует функцию
os.popen(), и к тому же цель этого модуля состоит в том, чтобы запустить
независимую программу, а не подключиться к ее потокам ввода-вывода;
можно было бы также использовать пакет multiprocessing, но данный модуль
предназначен для выполнения программ, а не функций: не имеет смысла запускать
процесс, когда можно использовать одну из уже имеющихся возможностей;
новое в этом издании: при запуске сценария передает путь к файлу сценария
через функцию normpath(), которая в Windows замещает все / на \; исправьте
соответствующие участки программного кода в PyEdit и в других сценариях;
вообще в Windows допускается использовать / в командах открытия файлов, но этот
символ может использоваться не во всех инструментах запуска программ;
"""

import sys
import os


PYFILE = (sys.platform[:3] == 'win' and 'python.exe') or 'python'
PYPATH = sys.executable     # Использовать sys в последних версиях python


def fix_windows_path(cmdline):
    r"""
    Замещает все / на \ в путях к сценариям в начале команд.
    Используется только классами, которые запускают инструменты, требующие
    этого в Windows, в других системах в этом нет необходимости (например,
    os.system в UNIX)
    """
    splitline = cmdline.lstrip().split(' ')       # Разбить по пробелам
    fixedpath = os.path.normpath(splitline[0])    # Заменить прямые слэши
    return ' '.join([fixedpath] + splitline[1:])  # Снова объединить в строку


class LaunchMode:
    """
    При вызове экземпляра класса выводится метка и запускается команда.
    Подклассы форматируют строки команд для run(), если необходимо.
    Команда должна начинаться с имени запускаемого файла сценария Python
    и не должна начинаться со слова 'python' или с полного пути к нему.
    """
    def __init__(self, label, command):
        self.what = label
        self.where = command

    def __call__(self):             # Вызывается при вызове экземпляра,
        self.announce(self.what)    # например как обработчик щелчка на кнопке
        self.run(self.where)        # подклассы должны определять метод run()

    def announce(self, text):       # подклассы могут переопределять метод
        print(text)                 # announce() вместо логики if/elif

    def run(self, cmdline):
        assert False, 'run must be defined'


class System(LaunchMode):
    """
    Запускает сценарий Python, указанный в команде оболочки.
    Внимание: может блокировать вызывающую программу,
    если в UNIX не добавить &
    """
    def run(self, cmdline):
        cmdline = fix_windows_path(cmdline)
        os.system(f'{PYPATH} {cmdline}')


class Popen(LaunchMode):
    """
    Запускает команду оболочки в новом процессе.
    Внимание: может блокировать вызывающую программу, потому что
    канал закрывается немедленно.
    """
    def run(self, cmdline):
        cmdline = fix_windows_path(cmdline)
        os.popen(PYPATH + ' ' + cmdline)    # Предполагается, что нет данных
                                            # для чтения


class Fork(LaunchMode):
    """
    Запускае команду в явно созданном новом процессе.
    Только для UNIX-подобных систем, включая cygwin
    """
    def run(self, cmdline):
        assert hasattr(os, 'fork')
        cmdline = cmdline.split()           # Превратить строку в список
        if os.fork() == 0:                  # Запустить новый процесс
            os.execvp(PYPATH, [PYFILE] + cmdline)     # и новую программу


class Start(LaunchMode):
    """
    Запускает команду, независимую от вызывающего процесса
    только для Windows: использует ассоциации с расширениями имён файлов
    """
    def run(self, cmdline):
        assert sys.platform[:3] == 'win'
        cmdline = fix_windows_path(cmdline)
        os.startfile(cmdline)


class StartArgs(LaunchMode):
    """
    Только для Windows: в аргументах могут присутствовать символы прямого
    слэша.
    """
    def run(self, cmdline):
        assert sys.platform[:3] == 'win'
        os.system('start ' + cmdline)       # Может создать окно консоли.


class Spawn(LaunchMode):
    """
    Запускает python в новом процессе, независимом от вызывающего,
    для Windows и Unix. Для окна dos используйте P_NOWAIT,
    символы прямого слеша допустимы.
    """
    def run(self, cmdline):
        os.spawnv(os.P_DETACH, PYPATH, (PYFILE, cmdline))


class TopLevel(LaunchMode):
    """
    Запускает тот же процесс в новом окне
    на будущее: требуется информация о классе графического интерфейса
    """
    def run(self, cmdline):
        assert False, 'Sorry - mode not yet implemented'

#
# выбор "лучшего средства запуска для данной платформы
# возможно, выбор придётся уточнить в других местах
#


if sys.platform[:3] == 'win':
    PortableLauncher = Spawn
else:
    PortableLauncher = Fork


class QuietPortableLauncher(PortableLauncher):
    def announce(self, text):
        pass


def self_test():
    file = 'echo.py'
    input('default mode...')
    launcher = PortableLauncher(file, file)
    launcher()                          # Не блокирует

    input('system mode...')
    System(file, file)()                # Блокирует

    if sys.platform[:3] == 'win':
        input('DOS tart mode...')       # не блокирует
        StartArgs(file, file)()


if __name__ == '__main__':
    self_test()
