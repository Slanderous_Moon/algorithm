import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId

import datetime
import pprint
import sys


# Веб-приложение получает post_id из URl и передаёт его как строку.
def get_document(post_id, database):
    # Конвертация post_id из строки в ObjectId
    return database.find_one({'_id': ObjectId(post_id)})


if __name__ == '__main__':

    # Создание экземпляра mongod
    # Подключение к дефолтному хосту и порту:
    # client = MongoClient()

    # Указать хост и порт вручную
    client = MongoClient('localhost', 27017)

    # Указать путь к Монге при помощи URL
    # client = MongoClient('mongodb://localhost:27017/')

    # Единственный экземпляр MongoDB умеет поддерживать несколько
    # независимых баз данных
    # Доступ к базе данных через атрибут:
    db = client.test_database
    # Если имя базы данных не позволяет обратиться к ней
    # как к атибуту, можно использовать "словарный стиль"
    # db = client['test-database']

    '''
    Коллекции - это группы документов, которые хранятся в МонгоДБ,
    представляющие собой некий эквивалент таблиц в РБД. 
    '''
    collection = db.test_collection
    # Или в "словарном стиле" db['test-collection']

    # Документы
    '''
    Данные в МонгоДБ хранятся в JSON-подобных докуменах. 
    Для представления документов MongoDB в PyMongo используются словари.
    Документы могут содержать в себе экземпляры встроенных в Pyhton типов,
    которые будут конвертированы в BSON-тип, однако экземпляры 
    пользовательских классов придётся "пиклить" (упаковывать при помощи
    модуля pickle). 
    '''
    post = {'author': 'Filthy Frank',
            'text': "ORE WA CHIN-CHIN GA DAISUKI NANDAYO!",
            'tags': ['Sir Francis of the Filth', 'misc'],
            'date': datetime.datetime.utcnow()}

    # Вставка документа:
    posts = db.posts
    # вставка с автоматическим присваиванием id документа переменной
    post_id = posts.insert_one(post).inserted_id
    print(post_id)
    # Можно было и вот так:
    # posts.insert_one(post)
    # post_id = posts.find_one({'author': 'paul'})
    # однако вышеописанный способ гораздо проще.

    # Извлечение одного документа
    '''
    find_one() - один из наиболее частых запросов к MongoDB.
    Метод возвращает один документ в соответствии с запросом 
    (если подходящих несколько - то первый найденный) или 
    None, если совпадений нет. 
    '''
    # Модуль pprint служит для наглядного вывода различных объектов
    pprint.pprint(posts.find_one())  # В базе всего один документ
    # Поиск по шаблону
    pprint.pprint(posts.find_one({'author': 'Filthy Frank'}))
    pprint.pprint(posts.find_one({'author': 'IdubbbzTV'}))  # Совпадений нет

    # Запросы по ObjectId
    '''
    Объекты можно искать так же и по _id. ObjectId имеет отдельное
    строковое представление. Это не одно и то же.
    '''
    pprint.pprint(post_id)
    pprint.pprint(posts.find_one({'_id': post_id}))

    post_id_as_str = str(post_id)
    print(post_id_as_str)
    posts.find_one({"id": post_id_as_str})  # Нет совпадений

    '''
    Одна из самых частых задач при работе веб-приложений - это извлечение
    ObjectId из сетевого запроса и поиск подходящего документа.
    В таком случае, перед передачей аргумента методу find_one, его нужно
    конвертировать в ObjectID.
    '''
    pprint.pprint(get_document(post_id_as_str, posts))

    # Множественные вставки
    '''
    Осущесвляются при помощи метода .insert_many()
    Аргументы передаются кортежем либо списком.
    '''
    new_posts = ({'author': 'Kenshiro',
                  'text': 'OWAE MA MOU SHINDEIRU!',
                  'tags': ['bulk', 'insert', 'NANI?!'],
                  'date': datetime.datetime.utcnow()},
                 {'author': 'Dio',
                  'text': 'ZA WARUDO!',
                  'tags': ['bulk', 'insert', 'WRYYYYY!'],
                  'date': datetime.datetime.utcnow()})
    result = posts.insert_many(new_posts).inserted_ids
    print(result)

    # Множественный поиск
    '''
    Осуществляется посредством метода .find(). Возвращает
    итерируемый объект Cursor. 
    '''
    # Итерация по всем объектам в posts:
    for record in posts.find():
        pprint.pprint(record)

    # Подсчёт:
    print(posts.count_documents({}),
          posts.count_documents({'author': 'Filthy Frank'}))

    # Расширенные запросы:
    d = datetime.datetime(2019, 8, 1, 10)

    for record in posts.find({'date': {'$lt': d}}).sort('author'):
        pprint.pprint(record)

    # Индексация
    '''
    Добавление указателей может помочь ускорить некоторые запросы,
    а так же может расширить функциональность базы данных. 
    '''
    result = db.profiles.create_index([('user_id', pymongo.ASCENDING)],
                                        unique=True)

    print(sorted(list(db.profiles.index_information())))

    # Учётные записи пользователей:

    try:
        user_profiles = [{'user_id': 211, 'name': 'Papa Franku'},
                         {'user_id': 212, 'name': 'mr. Negi'}]
        result = db.profiles.insert_many(user_profiles)
    except pymongo.errors.BulkWriteError as bwe:
        pprint.pprint(bwe.details, stream=sys.stderr)

    try:
        # Индексация предотвращает вставку документа, чей id уже в коллекции:
        new_profile = {'user_id': 213, 'name': 'mr. Negi Generation 3000'}
        dup_profile = {'user_id': 212, 'name': 'Evil Dade'}
        result = db.profiles.insert_one(new_profile)   # Такой запрос пройдёт
        result = db.profiles.insert_one(dup_profile)   # А такой - нет
    except pymongo.errors.DuplicateKeyError as dke:
        pprint.pprint(dke.details, stream=sys.stderr)


