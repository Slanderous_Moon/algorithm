import pymongo
import datetime
import os
import pprint
import copy
import re


USER = os.environ["MONGO_USER"]
PASSWORD = os.environ["MONGO_PASSWORD"]
AUTH_STRING = f'{USER}:{PASSWORD}'
MONGO_CONNECTION_STRING = 'mongodb://{}@ps{}.ntvk1.ru:27019/'
DATABASE = "creatives"
STAT_COLLECTION = 'stats_collection'
LEAD_COLLECTION = 'lead_collection'
PRODUCTION_SERVERS = 4
STAT_FORMS = ('1', '2', '3')


class WrongTimeStringError(TypeError):
    pass


def connect_to_mongo(_id):
    """
    Creates connection to NTVK mongodb chosen by _id.
    :param _id: string or int, index of the required mongodb
        example: 1, 2, 3, 4, 'h1'
    :return: pymongo.MongoClient object
    """
    client = pymongo.MongoClient(
        MONGO_CONNECTION_STRING.format(AUTH_STRING, _id)
    )
    return client


def access_to_mongo_collection(client, *,
                               req_db=DATABASE,
                               req_coll=LEAD_COLLECTION):
    """
    Returns link to the chosen mongo collection.
    :param client: pymongo.MongoClient object
    :param req_db: required database, is written in DATABASE
        constant by default
    :param req_coll: required collection, is written in COLLECTION
        constant by default
    :return: required collection object
    """
    collection = client[req_db][req_coll]
    return collection


def normalize_servers_argument(servers):
    """
    Supporting function that checks, whether given argument
    is string, integer or not. If not, returns given argument
    without any changes, else returns 1-element-tuple build from
    the given argument.
    :param servers: any
    :return: tuple if servers argument is str or list, else
        servers without change.
    """
    if isinstance(servers, int) or isinstance(servers, str):
        res = (servers, )
    else:
        res = servers
    return res


def normalize_datetime_string(dt):
    """
    Ascillary function.
    Accepts string of format "YYYY-MM-DD[ hh:mm:[ss]]"
    (examples: "2015-09-13 21:20:02", "2016-10-17 21:20",
    "2045-04-15"). Raises WrongTimeStringError if
    string given is not one of three given forms.
    Converts it to datetime.datetime object
    :param dt: String of format "YYYY-MM-DD[ hh:mm:[ss]]"
    :return: string of the format "YYYY-MM-DD hh:mm:ss"
    """
    if not re.match(r'\d\d\d\d-\d\d-\d\d', dt):
        raise WrongTimeStringError

    dt_lst = dt.split(' ')
    if len(dt_lst) == 1:
        res = dt + ' 00:00:00'
    else:
        if not re.fullmatch(r'(\d\d:\d\d)|(\d\d:\d\d:\d\d)', dt_lst[1]):
            raise WrongTimeStringError
        time_lst = dt_lst[1].split(':')
        if len(time_lst) == 2:
            res = dt + ':00'
        else:
            res = dt
    return res


def count_leads(query=None, *,
                servers=PRODUCTION_SERVERS,
                prnt=False,
                later_than=None,
                earlier_than=None):
    """
    Counts lead requests on ps4 server.
    :param query: pymongo filters, is dictionary.
    :param servers: required servers, list or tuple. Insert 'h1'
        to the iterable to have access to test server.
    :param prnt: Is False by default, prints result if True
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later,
        than "YYYY-MM-DD[ hh:mm:[ss]]"
        :examples: "2019-09-23 21:01:41", "2019-09-23 21:01",
                   "2019-09-23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier,
        than "YYYY-MM-DD[ hh:mm:[ss]]"
        :examples: "2019-09-23 21:01:41", "2019-09-23 21:01",
                   "2019-09-23"
    :return: dict, keys - server names, values - counted pages
    """
    if not query:
        query = {}
    if later_than or earlier_than:
        query['date_added'] = {}
        if later_than:
            later_than = normalize_datetime_string(later_than)
            query['date_added']['$gt'] = later_than
        if earlier_than:
            earlier_than = normalize_datetime_string(earlier_than)
            query['date_added']['$lt'] = earlier_than
    servers = normalize_servers_argument(servers)
    res = {}

    if prnt:
        print("*" * 100)
        print("Trying to count leads on the following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id)
        )
        res[f'ps{server_id}'] = pages.count_documents(query)

    if prnt:
        print('\n')
        print('Result:')
        pprint.pprint(res)
        print("*" * 100, "\n" * 5)

    return res


def find_leads(query=None, *,
               servers=PRODUCTION_SERVERS,
               limit=None,
               prnt=False,
               later_than=None,
               earlier_than=None):
    """
    Finds all (or limited number of) the leads on the ps4 server in
    accordance with the query.
    :param query: pymongo filters, is dictionary.
    :param servers: required servers, list or tuple.
    :param limit: limit the number of leads returned/printed
        by this value
    :param prnt: print found pages if True. False by default.
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later,
        than "YYYY-MM-DD[ hh:mm:[ss]]"
        :examples: "2019-09-23 21:01:41", "2019-09-23 21:01",
                   "2019-09-23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier,
        than "YYYY-MM-DD[ hh:mm:[ss]]"
        :examples: "2019-09-23 21:01:41", "2019-09-23 21:01",
                   "2019-09-23"
    :return: dict with server names as keys and Cursor objects
        containing found pages as values.
    """
    if not query:
        query = {}
    if later_than or earlier_than:
        query['date_added'] = {}
        if later_than:
            later_than = normalize_datetime_string(later_than)
            query['date_added']['$gt'] = later_than
        if earlier_than:
            earlier_than = normalize_datetime_string(earlier_than)
            query['date_added']['$lt'] = earlier_than
    servers = normalize_servers_argument(servers)
    found_pages = {}

    if prnt:
        print("*" * 100, )
        quantity = limit or ''
        servers_str = ', '.join([f'ps{server_id}' for server_id in servers])
        print(f"Searching through {servers_str} for {quantity} pages "
              "on the following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id)
        )
        if limit:
            found_pages[f'ps{server_id}'] = pages.find(query).limit(limit)
        else:
            found_pages[f'ps{server_id}'] = pages.find(query)

    if prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n", f"LEAD DOCUMENT FOUND ON SERVER ps{server_id}:\n",
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            for page in found_pages[f'ps{server_id}']:
                pprint.pprint(page)
                print("-" * 100)
        print("*" * 100, "\n" * 5)

    return found_pages


def count_stats(query=None, *,
                servers=PRODUCTION_SERVERS,
                prnt=False,
                forms_separately=False,
                later_than=None,
                earlier_than=None):
    """
    Counts stats on ps4 server.
    :param query: pymongo filters, is dictionary.
    :param servers: required servers, list or tuple. Insert 'h1'
        to the iterable to have access to test server.
    :param prnt: Is False by default, prints result if True
    :param forms_separately: if True, counts quantity of stat requests on
        each of three forms separately.
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later,
        than "YYYY-MM-DD[ hh:mm:[ss]]"
        :examples: "2019-09-23 21:01:41", "2019-09-23 21:01",
                   "2019-09-23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier,
        than "YYYY-MM-DD[ hh:mm:[ss]]"
        :examples: "2019-09-23 21:01:41", "2019-09-23 21:01",
                   "2019-09-23"
    :return: dict, keys - server names, values - counted pages
    """
    if not query:
        query = {}
    if later_than or earlier_than:
        query['date_added'] = {}
        if later_than:
            later_than = normalize_datetime_string(later_than)
            query['date_added']['$gt'] = later_than
        if earlier_than:
            earlier_than = normalize_datetime_string(earlier_than)
            query['date_added']['$lt'] = earlier_than
    servers = normalize_servers_argument(servers)
    res = {}

    if prnt:
        print("*" * 100)
        print("Trying to count leads on the following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id),
            req_coll=STAT_COLLECTION
        )
        counters = {}
        counters['total'] = pages.count_documents(query)
        if forms_separately:
            for form_num in STAT_FORMS:
                form_query = copy.deepcopy(query)
                form_query['form'] = form_num
                counters[f'form {form_num}'] = pages.count_documents(
                    form_query)
        res[f'ps{server_id}'] = counters

    if prnt:
        print('\n')
        print('Result:')
        pprint.pprint(res)
        print("*" * 100, "\n" * 5)

    return res


def find_stats(query=None, *,
               servers=PRODUCTION_SERVERS,
               limit=None,
               prnt=False,
               later_than=None,
               earlier_than=None):
    """
    Finds all (or limited number of) the stats on the ps4 server in
    accordance with the query.
    :param query: pymongo filters, is dictionary.
    :param servers: required servers, list or tuple.
    :param limit: limit the number of stats returned/printed
        by this value
    :param prnt: print found pages if True. False by default.
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later,
        than "YYYY-MM-DD[ hh:mm:[ss]]"
        :examples: "2019-09-23 21:01:41", "2019-09-23 21:01",
                   "2019-09-23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier,
        than "YYYY-MM-DD[ hh:mm:[ss]]"
        :examples: "2019-09-23 21:01:41", "2019-09-23 21:01",
                   "2019-09-23"
    :return: dict with server names as keys and Cursor objects
        containing found pages as values.
    """
    if not query:
        query = {}
    if later_than or earlier_than:
        query['date_added'] = {}
        if later_than:
            later_than = normalize_datetime_string(later_than)
            query['date_added']['$gt'] = later_than
        if earlier_than:
            earlier_than = normalize_datetime_string(earlier_than)
            query['date_added']['$lt'] = earlier_than
    servers = normalize_servers_argument(servers)
    found_pages = {}

    if prnt:
        print("*" * 100, )
        quantity = limit or ''
        servers_str = ', '.join([f'ps{server_id}' for server_id in servers])
        print(f"Searching through {servers_str} for {quantity} pages "
              "on the following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id),
            req_coll=STAT_COLLECTION
        )
        if limit:
            found_pages[f'ps{server_id}'] = pages.find(query).limit(limit)
        else:
            found_pages[f'ps{server_id}'] = pages.find(query)

    if prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n", f"STAT DOCUMENTS FOUND ON SERVER ps{server_id}:\n",
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            for page in found_pages[f'ps{server_id}']:
                pprint.pprint(page)
                print("-" * 100)
        print("*" * 100, "\n" * 5)

    return found_pages


def count_coverage(query=None, **kwargs):
    """
    Counts coverage i.e. quantity of form-1 stat requests
    from unique ip-s during given time period.
    :param query: pymongo filters, dict.
    **kwargs:
    :later_than: additional temporal parameter. Filters
        pages that were added to database later,
        than "YYYY-MM-DD[ hh:mm:[ss]]"
        :examples: "2019-09-23 21:01:41", "2019-09-23 21:01",
                   "2019-09-23"
    :earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier,
        than "YYYY-MM-DD[ hh:mm:[ss]]"
        :examples: "2019-09-23 21:01:41", "2019-09-23 21:01",
                   "2019-09-23"
    :return: coverage - int
    """
    if not query:
        query = {'form': '1'}
    form_1_stats = find_stats(query, **kwargs)['ps4']
    ips = []
    for page in form_1_stats:
        ips.append(page['request_ip'])
    unique_ips = set(ips)
    return len(unique_ips)
