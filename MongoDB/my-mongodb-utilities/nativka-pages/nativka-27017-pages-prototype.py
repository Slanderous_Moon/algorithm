import pymongo
import pprint
import random
import hashlib
import datetime


AUTH_STRING = 'localhost'
MONGO_CONNECTION_STRING = 'mongodb://{}:27017/'
DATABASE = "nativka"
COLLECTION = "pages"
PRODUCTION_SERVERS = 1

WEBSITES = ('libgen.is', 'habr.com', 'python.org',
            'yandex.ru', 'github.com', 'gitlab.com',
            'aruba.it', 'youtube.com', 'amazon.com',
            'docker.com', 'proglib.io', 'hackerrank.com',
            'regex101.com', 'codewars.com', 'topdeck.ru',
            'thepiratebay.com', 'starcitygames.com', 'mtggoldfish.com',
            'wikipedia.org', 'mythicspoiler.com', 'mtgtop8.com',
            'freeones.ru', 'stackoverflow.com', 'mongodb.com')
METHOD = ('http', 'https')
RANDOM_WORDS = ('library', 'databases', 'miscellaneous'
                'pages', 'news', 'guts',
                'torrent', 'partners', 'set',
                'rope', 'queries', 'questions',
                'accumulation', 'topics', 'summertime',
                'weight', 'examples', 'treasures',
                'landing', 'pokemons', 'stories',
                'eye', 'hole', 'integer')
RANDOM_MARK = ('suppressed', 'prepared')


class EmptyQueryError(TypeError):
    pass


def connect_to_mongo():
    """
    Creates connection to NTVK mongodb chosen by id.
    :return: pymongo.MongoClient object
    """
    client = pymongo.MongoClient(
        MONGO_CONNECTION_STRING.format(AUTH_STRING)
    )
    return client


def access_to_mongo_collection(client, *,
                               req_db=DATABASE,
                               req_coll=COLLECTION):
    """
    Returns link to the chosen mongo collection.
    :param client: pymongo.MongoClient object
    :param req_db: required database, is written in DATABASE
        constant by default
    :param req_coll: required collection, is written in COLLECTION
        constant by default
    :return: required collection object
    """
    collection = client[req_db][req_coll]
    return collection


def normalize_servers_argument(servers):
    """
    Supporting function, that checks, whether given argument
    is string, integer or not. If not, returns given argument
    without any changes, else returns 1-element-tuple build from
    the given argument.
    :param servers: any
    :return: tuple if servers argument is str or list, else
        servers without change.
    """
    if isinstance(servers, int) or isinstance(servers, str):
        res = (servers, )
    else:
        res = servers
    return res


def convert_datetime_string(dt):
    """
    Accepts string of format "DD.MM.YYYY[ hh:mm]"
    (examples: "2015.06.13 21:20", "2045.04.15")
    Converts it to datetime.datetime object
    :param dt: String of format "DD.MM.YYYY[ hh:mm]"
    :return: datetime.datetime obj
    """
    dt_lst = dt.split(' ')
    if len(dt_lst) > 1:
        date_str, time_str = dt_lst
    else:
        date_str, time_str = dt_lst[0], ''
    year, month, day = date_str.split('.')
    if time_str:
        time_lst = time_str.split(':')
        hours, minutes = time_lst
    else:
        hours, minutes = '00', '00'
    datetime_tuple = tuple(map(int, [year, month, day, hours, minutes]))
    return datetime.datetime(*datetime_tuple)


def count_pages(query=None, *,
                servers=PRODUCTION_SERVERS,
                prnt=False,
                later_than=None,
                earlier_than=None):
    """
    Counts required pages on the chosen server(s).
    All the production servers (ps1, ps2, ps3) are chosen by default.
    :param query: pymongo filters, is dictionary.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: Is False by default, prints result if True
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    :return: dict, keys - server names, values - counted pages
    """
    if not query:
        query = {}
    if later_than or earlier_than:
        query['dt_added'] = {}
        if later_than:
            query['dt_added']['$gt'] = convert_datetime_string(later_than)
        if earlier_than:
            query['dt_added']['$lt'] = convert_datetime_string(earlier_than)
    servers = normalize_servers_argument(servers)
    res = {}

    if prnt:
        print("*" * 100)
        print("Trying to count pages on the following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo()
        )
        res[f'ps{server_id}'] = pages.count_documents(query)

    if prnt:
        print('\n')
        print('Result:')
        pprint.pprint(res)
        print("*" * 100, "\n" * 5)

    return res


def find_pages(query=None, *,
               servers=PRODUCTION_SERVERS,
               limit=None,
               prnt=False,
               expanded_prnt=False,
               later_than=None,
               earlier_than=None):
    """
    Finds all (or limited number of) the pages on the chosen
    server(s) in accordance with the given query. All the production
    servers are chosen by default.
    :param query: pymongo filters, is dictionary.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param limit: limit the number of pages returned/printed
        by this value
    :param prnt: print ids of the pages found if prnt=True.
        False by default.
    :param expanded_prnt: print found pages if True. False by default.
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    :return: dict with server names as keys and Cursor objects
        containing found pages as values.
    """
    if not query:
        query = {}
    if later_than or earlier_than:
        query['dt_added'] = {}
        if later_than:
            query['dt_added']['$gt'] = convert_datetime_string(later_than)
        if earlier_than:
            query['dt_added']['$lt'] = convert_datetime_string(earlier_than)
    servers = normalize_servers_argument(servers)
    found_pages = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        quantity = limit or ''
        servers_str = ', '.join([f'ps{server_id}' for server_id in servers])
        print(f"Searching through {servers_str} for {quantity} pages "
              "on the following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo()
        )
        if limit:
            found_pages[f'ps{server_id}'] = pages.find(query).limit(limit)
        else:
            found_pages[f'ps{server_id}'] = pages.find(query)

    if prnt or expanded_prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n", f"PAGES FOUND ON SERVER ps{server_id}:\n",
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            for page in found_pages[f'ps{server_id}']:
                pprint.pprint(page if expanded_prnt else page['_id'])
                print("-" * 100)
        print("*" * 100, "\n" * 5)

    return found_pages


def delete_pages(query, *,
                 servers=PRODUCTION_SERVERS,
                 prnt=False,
                 expanded_prnt=False,
                 later_than=None,
                 earlier_than=None):
    """
    Deletes and returns all the pages on the chosen servers
    in accordance with the given query. All the production
    servers are chosen by default.
    :param query: pymongo filters. Set this parameter to "all"
        if it's necessary to delete all pages.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages deleted if True.
        False by default.
    :param expanded_prnt: print deleted pages if True.
        False by default.
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    return: dict with server names as keys and Cursor objects
        containing deleted pages as values.
    """
    if not query:
        raise EmptyQueryError
    elif query == 'all':
        query = {}
    if later_than or earlier_than:
        query['dt_added'] = {}
        if later_than:
            query['dt_added']['$gt'] = convert_datetime_string(later_than)
        if earlier_than:
            query['dt_added']['$lt'] = convert_datetime_string(earlier_than)
    servers = normalize_servers_argument(servers)
    deleted = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{server_id}' for server_id in servers])
        print(f"Deleting pages from {servers_str} on the following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo()
        )
        deleted[f'ps{server_id}'] = pages.find(query)

        if expanded_prnt:
            print('\n' * 2, f'Deleting following pages out of ps{server_id}:',
                  '\n' * 2, "-" * 100)

        deleted_counter = 0
        for page in deleted[f'ps{server_id}']:
            if expanded_prnt:
                print("-" * 100)
                pprint.pprint(page)
                print("-" * 100)
            pages.delete_one({'_id': page['_id']})
            deleted_counter += 1
        deleted[f'ps{server_id}_counter'] = deleted_counter

    if prnt or expanded_prnt:
        print('\n' * 2, 'Result:')
        print('~' * 27, '\n', 'DELETED TOTAL:')
        for server_id in servers:
            print(f'ps{server_id}: ',
                  deleted[f'ps{server_id}_counter'])
        print("*" * 100, "\n" * 5)

    for server_id in servers:
        del deleted[f'ps{server_id}_counter']
    return deleted


def insert_pages(documents, *,
                 servers=PRODUCTION_SERVERS,
                 prnt=False):
    """
    Insert and return given documents to 'pages' collection on
    the chosen servers. All the production servers are chosen
    by default.
    :param documents: an iterable of documets to insert.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages inserted if True.
        False by default.
    return: dict with server names as keys and Cursor objects
        containing inserted pages as values.
    """
    servers = normalize_servers_argument(servers)
    inserted = {}

    if prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{server_id}' for server_id in servers])
        print(f"Inserting documents to 'pages' collections of {servers_str}.")

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo()
        )
        inserted[f'ps{server_id}'] = pages.insert_many(documents)

    if prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n", f"INSERTED TO ps{server_id}:\n",
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            pprint.pprint(inserted[f'ps{server_id}'].inserted_ids)
        print("*" * 100, "\n" * 5)

    return inserted


def suppress_pages(query=None, *,
                   servers=PRODUCTION_SERVERS,
                   prnt=False,
                   expanded_prnt=False,
                   later_than=None,
                   earlier_than=None):
    """
    Finds pages in accordance with the query and sets
    "suppressed" flags to True on the found pages.
    All the production servers are chosen by default.
    :param query: pymongo filters, shows, which pages
        should be suppressed.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages suppressed if True.
        False by default.
    :param expanded_prnt: print suppressed pages if True.
        False by default.
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    return: None.
    """
    if not query:
        query = {}
    if later_than or earlier_than:
        query['dt_added'] = {}
        if later_than:
            query['dt_added']['$gt'] = convert_datetime_string(later_than)
        if earlier_than:
            query['dt_added']['$lt'] = convert_datetime_string(earlier_than)
    servers = normalize_servers_argument(servers)
    suppressed = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{server_id}' for server_id in servers])
        print(f"Suppressing pages on {servers_str}"
              " in accordance with following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo()
        )
        suppressed[f'ps{server_id}'] = pages.update_many(
            query,
            {'$set': {'suppressed': True}}
        )
        suppressed[f'ps{server_id}'] = pages.find(query)

        if expanded_prnt:
            print('\n' * 2, 'Setting "suppressed" field to "True"',
                  f'on ps{server_id} on the following pages:',
                  '\n' * 2, "-" * 100)

        suppressed_counter = 0
        for page in suppressed[f'ps{server_id}']:
            pages.update_one({'_id': page['_id']},
                             {'$set': {'suppressed': True}})
            if expanded_prnt:
                print("-" * 100)
                pprint.pprint(page)
                print("-" * 100)
            suppressed_counter += 1
        suppressed[f'ps{server_id}_counter'] = suppressed_counter

    if prnt or expanded_prnt:
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n" * 2,
                  f"SUPPRESSED TOTAL ON ps{server_id}: ",
                  suppressed[f'ps{server_id}_counter'],
                  "\n" * 2,
                  "~" * 27)
        print("*" * 100, "\n" * 5)


def unsuppress_pages(query=None, *,
                     servers=PRODUCTION_SERVERS,
                     prnt=False,
                     expanded_prnt=False,
                     later_than=None,
                     earlier_than=None):
    """
    Adds "suppressed": True field to the query,
    finds pages in accordance with the modified query and
    sets "suppressed" flags to False on the found pages.
    All the production servers are chosen by default.
    :param query: pymongo filters, shows, which pages
        should be unsuppressed.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages unsuppressed if True.
        False by default.
    :param expanded_prnt: print unsuppressed pages if True.
        False by default.
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    return: None
    """
    if not query:
        query = {}
    if later_than or earlier_than:
        query['dt_added'] = {}
        if later_than:
            query['dt_added']['$gt'] = convert_datetime_string(later_than)
        if earlier_than:
            query['dt_added']['$lt'] = convert_datetime_string(earlier_than)
    if not query.get('suppressed'):
        query['suppressed'] = True
    servers = normalize_servers_argument(servers)
    unsuppressed = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{server_id}' for server_id in servers])
        print(f"Unsuppressing pages on {servers_str}"
              " in accordance with following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo()
        )
        unsuppressed[f'ps{server_id}'] = pages.find(query)
        if expanded_prnt:
            print('\n' * 2, 'Setting "suppressed" field to "False"',
                  f'on ps{server_id} on the following pages:',
                  '\n' * 2, "-" * 100)

        unsuppressed_counter = 0
        for page in unsuppressed[f'ps{server_id}']:
            pages.update_one({'_id': page['_id']},
                             {'$set': {'suppressed': False}})
            if expanded_prnt:
                pprint.pprint(page)
            unsuppressed_counter += 1
        unsuppressed[f'ps{server_id}_counter'] = unsuppressed_counter

    if prnt or expanded_prnt:
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n" * 2,
                  f"UNSUPPRESSED TOTAL ON ps{server_id}: ",
                  unsuppressed[f'ps{server_id}_counter'],
                  "\n" * 2,
                  "~" * 27)
        print("*" * 100, "\n" * 5)


def prepare_pages(query=None, *,
                  servers=PRODUCTION_SERVERS,
                  prnt=False,
                  expanded_prnt=False,
                  later_than=None,
                  earlier_than=None):
    """
    Finds pages in accordance with the query and sets
    "prepared" flags to True on the found pages.
    All the production servers are chosen by default.
    :param query: pymongo filters, shows, which pages
        should be prepared.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages prepared if True.
        False by default.
    :param expanded_prnt: print prepared pages if True.
        False by default.
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    return: None.
    """
    if not query:
        query = {}
    if later_than or earlier_than:
        query['dt_added'] = {}
        if later_than:
            query['dt_added']['$gt'] = convert_datetime_string(later_than)
        if earlier_than:
            query['dt_added']['$lt'] = convert_datetime_string(earlier_than)
    servers = normalize_servers_argument(servers)
    prepared = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{server_id}' for server_id in servers])
        print(f"Preparing pages on {servers_str}"
              " in accordance with following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo()
        )
        prepared[f'ps{server_id}'] = pages.find(query)
        if expanded_prnt:
            print('\n' * 2, 'Setting "prepared" field to "True"',
                  f'on ps{server_id} on the following pages:',
                  '\n' * 2, "-" * 100)

        prepared_counter = 0
        for page in prepared[f'ps{server_id}']:
            pages.update_one({'_id': page['_id']},
                             {'$set': {'prepared': True}})
            if expanded_prnt:
                print("-" * 100)
                pprint.pprint(page)
                print("-" * 100)
            prepared_counter += 1
        prepared[f'ps{server_id}_counter'] = prepared_counter

    if prnt or expanded_prnt:
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n" * 2,
                  f"PREPARED TOTAL ON ps{server_id}: ",
                  prepared[f'ps{server_id}_counter'],
                  "\n" * 2,
                  "~" * 27)
        print("*" * 100, "\n" * 5)


def unprepare_pages(query=None, *,
                    servers=PRODUCTION_SERVERS,
                    prnt=False,
                    expanded_prnt=False,
                    later_than=None,
                    earlier_than=None):
    """
    Adds "prepared": True field to the query,
    finds pages in accordance with the modified query and
    sets "prepared" flags to False on the found pages.
    All the production servers are chosen by default.
    :param query: pymongo filters, shows, which pages
        should be unprepared.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages unprepared if True.
        False by default.
    :param expanded_prnt: print unprepared pages if True.
        False by default.
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    return: None
    """
    if not query:
        query = {}
    if later_than or earlier_than:
        query['dt_added'] = {}
        if later_than:
            query['dt_added']['$gt'] = convert_datetime_string(later_than)
        if earlier_than:
            query['dt_added']['$lt'] = convert_datetime_string(earlier_than)
    if not query.get('prepared'):
        query['prepared'] = True
    servers = normalize_servers_argument(servers)
    unprepared = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{server_id}' for server_id in servers])
        print(f"Unpreparing pages on {servers_str}"
              " in accordance with following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo()
        )
        unprepared[f'ps{server_id}'] = pages.find(query)
        if expanded_prnt:
            print('\n' * 2, 'Setting "prepared" field to "False"',
                  f'on ps{server_id} on the following pages:',
                  '\n' * 2, "-" * 100)

        unprepared_counter = 0
        for page in unprepared[f'ps{server_id}']:
            pages.update_one({'_id': page['_id']},
                             {'$set': {'prepared': False}})
            if expanded_prnt:
                pprint.pprint(page)
            unprepared_counter += 1
        unprepared[f'ps{server_id}_counter'] = unprepared_counter

    if prnt or expanded_prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n" * 2,
                  f"UNPREPARED TOTAL ON ps{server_id}: ",
                  unprepared[f'ps{server_id}_counter'],
                  "\n" * 2,
                  "~" * 27)
        print("*" * 100, "\n" * 5)


def remove_prepared_field(query=None, *,
                          servers=PRODUCTION_SERVERS,
                          prnt=False,
                          expanded_prnt=False,
                          later_than=None,
                          earlier_than=None):
    """
    Finds pages in accordance with the query and remove
    "prepared" field out of the document.
    All the production servers are chosen by default.
    :param query: pymongo filters, shows, out of which pages
        "prepared" field should be removed.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages with removed fields if
        True. False by default.
    :param expanded_prnt: print prepared pages if True.
        False by default.
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    return: None
    """
    if not query:
        query = {}
    if not query.get('prepared'):
        query['prepared'] = True
    if later_than or earlier_than:
        query['dt_added'] = {}
        if later_than:
            query['dt_added']['$gt'] = convert_datetime_string(later_than)
        if earlier_than:
            query['dt_added']['$lt'] = convert_datetime_string(earlier_than)
    servers = normalize_servers_argument(servers)
    rm_prepared = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{server_id}' for server_id in servers])
        print(f'Removing "prepared" field out of the pages on {servers_str}\n'
              'in accordance with following query:')
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(connect_to_mongo())
        rm_prepared[f'ps{server_id}'] = pages.find(query)
        if expanded_prnt:
            print('\n' * 2, f'Removed "prepared" field on ps{server_id}',
                  ' out of the following pages:', '\n' * 2, "-" * 100)

        rm_prepared_counter = 0
        for page in rm_prepared[f'ps{server_id}']:
            pages.update_one({'_id': page['_id']},
                             {'$unset': {'prepared': True}})
            if expanded_prnt:
                pprint.pprint(page)
            rm_prepared_counter += 1
        rm_prepared[f'ps{server_id}_counter'] = rm_prepared_counter

    if prnt or expanded_prnt:
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n" * 2,
                  f'TOTAL REMOVED "prepared" FIELDS ON ps{server_id}: ',
                  rm_prepared[f'ps{server_id}_counter'],
                  "\n" * 2,
                  "~" * 27)
        print("*" * 100, "\n" * 5)


def remove_suppressed_field(query=None, *,
                            servers=PRODUCTION_SERVERS,
                            prnt=False,
                            expanded_prnt=False,
                            later_than=None,
                            earlier_than=None):
    """
    Finds pages in accordance with the query and remove
    "suppressed" field out of the document.
    All the production servers are chosen by default.
    :param query: pymongo filters, shows, out of which pages
        "suppressed" field should be removed.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages with removed fields if
        True. False by default.
    :param expanded_prnt: print prepared pages if True.
        False by default.
    :param later_than: additional temporal parameter. Filters
        pages that were added to database later, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    :param earlier_than: additional temporal parameter. Filters
        pages that were added to database earlier, than "YYYY.MM.DD[ hh:mm]"
        :examples: "2019.09.23 21:01", "2019.09.23"
    return: None
    """
    if not query:
        query = {}
    if not query.get('suppressed'):
        query['suppressed'] = True
    if later_than or earlier_than:
        query['dt_added'] = {}
        if later_than:
            query['dt_added']['$gt'] = convert_datetime_string(later_than)
        if earlier_than:
            query['dt_added']['$lt'] = convert_datetime_string(earlier_than)
    servers = normalize_servers_argument(servers)
    rm_suppressed = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{server_id}' for server_id in servers])
        print('Removing "suppressed" field out of the pages on' 
              f'{servers_str}\n in accordance with following query:')
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(connect_to_mongo())
        rm_suppressed[f'ps{server_id}'] = pages.find(query)
        if expanded_prnt:
            print('\n' * 2, f'Removed "suppressed" field on ps{server_id}',
                  ' out of the following pages:', '\n' * 2, "-" * 100)

        rm_suppressed_counter = 0
        for page in rm_suppressed[f'ps{server_id}']:
            pages.update_one({'_id': page['_id']},
                             {'$unset': {'suppressed': True}})
            if expanded_prnt:
                pprint.pprint(page)
            rm_suppressed_counter += 1
        rm_suppressed[f'ps{server_id}_counter'] = rm_suppressed_counter

    if prnt or expanded_prnt:
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n" * 2,
                  f'TOTAL REMOVED "suppressed" FIELDS ON ps{server_id}: ',
                  rm_suppressed[f'ps{server_id}_counter'],
                  "\n" * 2,
                  "~" * 27)
        print("*" * 100, "\n" * 5)


def now():
    return datetime.datetime.now()


def generate_test_document():
    document = dict()
    document['website'] = random.choice(WEBSITES)
    document['url'] = (random.choice(METHOD) + '://' +
                       document['website'] + '/' +
                       random.choice(RANDOM_WORDS) + '/' +
                       str(random.randint(10000, 1000000)) + '/')
    document['doc_id'] = hashlib.sha224(
        document['url'].encode()
    ).hexdigest()
    if random.randint(0, 1):
        document[random.choice(RANDOM_MARK)] = True
    document['dt_added'] = now()
    return document


if __name__ == '__main__':
    pages = access_to_mongo_collection(
        connect_to_mongo()
    )
    count_pages({'website': 'docker.com'},
                later_than='2019.09.25 19:00',
                earlier_than='2019.09.25 21:00',
                prnt=True)
