import pymongo
import datetime
import os
import pprint

USER = os.environ["MONGO_USER"]
PASSWORD = os.environ["MONGO_PASSWORD"]
AUTH_STRING = f'{USER}:{PASSWORD}'
MONGO_CONNECTION_STRING = 'mongodb://{}@ps{}.ntvk1.ru:27017/'
DATABASE = "nativka"
COLLECTION = "pages"
PRODUCTION_SERVERS = (1, 2, 3)


def connect_to_mongo(id):
    """
    Creates connection to NTVK mongodb chosen by id.
    :param id: string or int, index of the required mongodb
        example: 1, 2, 3, 'h1'
    :return: pymongo.MongoClient object
    """
    client = pymongo.MongoClient(
        MONGO_CONNECTION_STRING.format(AUTH_STRING, id)
    )
    return client


def access_to_mongo_collection(client, *,
                               req_db=DATABASE,
                               req_coll=COLLECTION):
    """
    Returns link to the chosen mongo collection.
    :param client: pymongo.MongoClient object
    :param req_db: required database, is written in DATABASE
        constant by default
    :param req_coll: required collection, is written in COLLECTION
        constant by default
    :return: required collection object
    """
    collection = client[req_db][req_coll]
    return collection


def normalize_servers_argument(servers):
    """
    Supporting function, that checks, whether given argument
    is string, integer or not. If not, returns given argument
    without any changes, else returns 1-element-tuple build from
    the given argument.
    :param servers: any
    :return: tuple if servers argument is str or list, else
        servers without change.
    """
    if isinstance(servers, int) or isinstance(servers, str):
        res = (servers, )
    else:
        res = servers
    return res


def count_pages(query: dict, *,
                servers=PRODUCTION_SERVERS,
                prnt=False):
    """
    Counts required pages on the chosen server(s).
    All the production servers (ps1, ps2, ps3) are chosen by default.
    :param query: pymongo filters, is dictionary.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: Is False by default, prints result if True
    :return: dict, keys - server names, values - counted pages
    """
    servers = normalize_servers_argument(servers)
    res = {}

    if prnt:
        print("*" * 100)
        print("Trying to count pages on the following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id)
        )
        res[f'ps{server_id}'] = pages.count_documents(query)

    if prnt:
        print('\n')
        print('Result:')
        pprint.pprint(res)
        print("*" * 100, "\n" * 5)

    return res


def find_pages(query: dict, *,
               servers=PRODUCTION_SERVERS,
               limit=None,
               prnt=False,
               expanded_prnt=False):
    """
    Finds all (or limited number of) the pages on the chosen
    server(s) in accordance with the given query. All the production
    servers are chosen by default.
    :param query: pymongo filters, is dictionary.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param limit: limit the number of pages returned/printed
        by this value
    :param prnt: print ids of the pages found if prnt=True.
        False by default.
    :param expanded_prnt: print found pages if True. False by default.
    :return: dict with server names as keys and Cursor objects
        containing found pages as values.
    """
    servers = normalize_servers_argument(servers)
    found_pages = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        quantity = limit or ''
        servers_str = ', '.join([f'ps{id}' for id in servers])
        print(f"Searching through {servers_str} for {quantity} pages "
              "on the following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id)
        )
        if limit:
            found_pages[f'ps{server_id}'] = pages.find(query).limit(limit)
        else:
            found_pages[f'ps{server_id}'] = pages.find(query)

    if prnt or expanded_prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n", f"PAGES FOUND ON SERVER ps{server_id}:\n",
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            for page in found_pages[f'ps{server_id}']:
                pprint.pprint(page if expanded_prnt else page['_id'])
                print("-" * 100)
        print("*" * 100, "\n" * 5)

    return found_pages


# TO DO: Needs to be tested
def delete_pages(query: dict, *,
                 servers=PRODUCTION_SERVERS,
                 prnt=False,
                 expanded_prnt=False):
    """
    Deletes and returns all the pages on the chosen servers
    in accordace with the given query. All the production
    servers are chosen by default.
    :param query: pymongo filters.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages deleted if True.
        False by default.
    :param expanded_prnt: print deleted pages if True.
        False by default.
    return: dict with server names as keys and Cursor objects
        containing deleted pages as values.
    """
    servers = normalize_servers_argument(servers)
    deleted = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{id}' for id in servers])
        print(f"Deleting pages from {servers_str} on the following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id)
        )
        deleted[f'ps{server_id}'] = pages.delete_many(query)

    if prnt or expanded_prnt:
        print('\n' * 2, 'Result:')
        print('~' * 27, '\n', 'DELETED TOTAL:')
        for server_id in servers:
            print(f'ps{server_id}: ',
                  deleted[f'ps{server_id}'].deleted_count)
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n", f"DELETED FROM ps{server_id}:\n",
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            for page in deleted[f'ps{server_id}']:
                pprint.pprint(page if expanded_prnt else page['_id'])
                print("-" * 100)
        print("*" * 100, "\n" * 5)

    return deleted


# TO DO: Needs to be tested
def insert_pages(documents, *,
                 servers=PRODUCTION_SERVERS,
                 prnt=False,
                 expanded_prnt=False):
    """
    Insert and return specified documents to 'pages' collection on
    the chosen servers. All the production servers are chosen
    by default.
    :param documents: an iterable of documets to insert.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages inserted if True.
        False by default.
    :param expanded_prnt: print inserted pages if True.
        False by default.
    return: dict with server names as keys and Cursor objects
        containing inserted pages as values.
    """
    servers = normalize_servers_argument(servers)
    inserted = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{id}' for id in servers])
        print(f"Inserting documents to 'pages' collections of {servers_str}.")

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id)
        )
        inserted[f'ps{server_id}'] = pages.insert_many(documents)

    if prnt or expanded_prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n", f"INSERTED TO ps{server_id}:\n",
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            if expanded_prnt:
                for page in inserted[f'ps{server_id}']:
                    pprint.pprint(page)
                    print("-" * 100)
            else:
                pprint.pprint(inserted[f'ps{server_id}'].inserted_ids)
                print("-" * 100)

        print("*" * 100, "\n" * 5)

    return inserted


# TO DO: Needs to be tested
def suppress_pages(query, *,
                   servers=PRODUCTION_SERVERS,
                   prnt=False,
                   expanded_prnt=False):
    """
    Finds pages in accordance with the query and sets
    "suppressed" flags to True on the found pages.
    All the production servers are chosen by default.
    :param query: pymongo filters, shows, which pages
        should be suppressed.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages suppressed if True.
        False by default.
    :param expanded_prnt: print suppressed pages if True.
        False by default.
    return: dict with server names as keys and Cursor objects
        containing suppressed pages as values.
    """
    servers = normalize_servers_argument(servers)
    suppressed = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{id}' for id in servers])
        print(f"Suppressing pages on {servers_str}"
              " in accordance with following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id)
        )
        suppressed[f'ps{server_id}'] = pages.update_many(
            query,
            {'$set': {'suppressed': True}}
        )

    if prnt or expanded_prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n" * 2,
                  f"SUPPRESSED TOTAL ON ps{server_id}: ",
                  suppressed[f'ps{server_id}'].modified_count,
                  "\n" * 2,
                  "~" * 27,
                  "\n", f"SUPPRESSED ON ps{server_id}:\n",
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            for page in suppressed[f'ps{server_id}']:
                pprint.pprint(page if expanded_prnt else page['_id'])
                print("-" * 100)
        print("*" * 100, "\n" * 5)

    return suppressed


# TO DO: Needs to be tested:
def unsuppress_pages(query, *,
                     servers=PRODUCTION_SERVERS,
                     prnt=False,
                     expanded_prnt=False):
    """
    Adds "suppressed": True field to the query,
    finds pages in accordance with the modified query and
    sets "suppressed" flags to False on the found pages.
    All the production servers are chosen by default.
    :param query: pymongo filters, shows, which pages
        should be unsuppressed.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages unsuppressed if True.
        False by default.
    :param expanded_prnt: print unsuppressed pages if True.
        False by default.
    return: dict with server names as keys and Cursor objects
        containing unsuppressed pages as values.
    """
    servers = normalize_servers_argument(servers)
    unsuppressed = {}
    if not query.get('suppressed'):
        query['suppressed'] = True

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{id}' for id in servers])
        print(f"Unsuppressing pages on {servers_str}"
              " in accordance with following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id)
        )
        unsuppressed[f'ps{server_id}'] = pages.update_many(
            query,
            {'$set': {'suppressed': False}}
        )

    if prnt or expanded_prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n" * 2,
                  f"UNSUPPRESSED TOTAL ON ps{server_id}: ",
                  unsuppressed[f'ps{server_id}'].modified_count,
                  "\n" * 2,
                  "~" * 27,
                  "\n", f"UNSUPPRESSED ON ps{server_id}:\n",
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            for page in unsuppressed[f'ps{server_id}']:
                pprint.pprint(page if expanded_prnt else page['_id'])
                print("-" * 100)
        print("*" * 100, "\n" * 5)

    return unsuppressed


# TO DO: Needs to be tested
def prepare_pages(query, *,
                  servers=PRODUCTION_SERVERS,
                  prnt=False,
                  expanded_prnt=False):
    """
    Finds pages in accordance with the query and sets
    "prepared" flags to True on the found pages.
    All the production servers are chosen by default.
    :param query: pymongo filters, shows, which pages
        should be prepared.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages prepared if True.
        False by default.
    :param expanded_prnt: print prepared pages if True.
        False by default.
    return: dict with server names as keys and Cursor objects
        containing prepared pages as values.
    """
    servers = normalize_servers_argument(servers)
    prepared = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{id}' for id in servers])
        print(f"Preparing pages on {servers_str}"
              " in accordance with following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id)
        )
        prepared[f'ps{server_id}'] = pages.update_many(
            query,
            {'$set': {'prepared': True}}
        )

    if prnt or expanded_prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n" * 2,
                  f"PREPARED TOTAL ON ps{server_id}: ",
                  prepared[f'ps{server_id}'].modified_count,
                  "\n" * 2,
                  "~" * 27,
                  "\n", f"PREPARED ON ps{server_id}:\n",
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            for page in prepared[f'ps{server_id}']:
                pprint.pprint(page if expanded_prnt else page['_id'])
                print("-" * 100)
        print("*" * 100, "\n" * 5)

    return prepared


# TO DO: Needs to be tested
def unprepare_pages(query, *,
                    servers=PRODUCTION_SERVERS,
                    prnt=False,
                    expanded_prnt=False):
    """
    Adds "prepared": True field to the query,
    finds pages in accordance with the modified query and
    sets "prepared" flags to False on the found pages.
    All the production servers are chosen by default.
    :param query: pymongo filters, shows, which pages
        should be unprepared.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages unprepared if True.
        False by default.
    :param expanded_prnt: print unprepared pages if True.
        False by default.
    return: dict with server names as keys and Cursor objects
        containing unprepared pages as values.
    """
    servers = normalize_servers_argument(servers)
    unprepared = {}
    if not query.get('prepared'):
        query['prepared'] = True

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{id}' for id in servers])
        print(f"Unpreparing pages on {servers_str}"
              " in accordance with following query:")
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id)
        )
        unprepared[f'ps{server_id}'] = pages.update_many(
            query,
            {'$set': {'prepared': False}}
        )

    if prnt or expanded_prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n" * 2,
                  f"UNPREPARED TOTAL ON ps{server_id}: ",
                  unprepared[f'ps{server_id}'].modified_count,
                  "\n" * 2,
                  "~" * 27,
                  "\n", f"UNPREPARED ON ps{server_id}:\n",
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            for page in unprepared[f'ps{server_id}']:
                pprint.pprint(page if expanded_prnt else page['_id'])
                print("-" * 100)
        print("*" * 100, "\n" * 5)

    return unprepared


# TO DO: Needs to be tested
def remove_prepared_field(query, *,
                          servers=PRODUCTION_SERVERS,
                          prnt=False,
                          expanded_prnt=False):
    """
    Finds pages in accordance with the query and remove
    "prepared" field out of the document.
    All the production servers are chosen by default.
    :param query: pymongo filters, shows, out of which pages
        "prepared" field should be removed.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages with removed fields if
        True. False by default.
    :param expanded_prnt: print prepared pages if True.
        False by default.
    return: dict with server names as keys and Cursor objects
        containing pages with removed fields as values.
    """
    servers = normalize_servers_argument(servers)
    rm_prepared = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{id}' for id in servers])
        print(f'Removing "prepared" field out of the pages on {servers_str}\n'
              'in accordance with following query:')
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id)
        )
        rm_prepared[f'ps{server_id}'] = pages.update_many(
            query,
            {'$unset': {'prepared': True}}
        )

    if prnt or expanded_prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n" * 2,
                  f'TOTAL REMOVED "prepared" FIELDS ON ps{server_id}: ',
                  rm_prepared[f'ps{server_id}'].modified_count,
                  "\n" * 2,
                  "~" * 27,
                  "\n", f'REMOVED "prepared" FIELDS ON ps{server_id}:\n',
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            for page in rm_prepared[f'ps{server_id}']:
                pprint.pprint(page if expanded_prnt else page['_id'])
                print("-" * 100)
        print("*" * 100, "\n" * 5)

    return rm_prepared


# TO DO: Needs to be tested
def remove_suppressed_field(query, *,
                            servers=PRODUCTION_SERVERS,
                            prnt=False,
                            expanded_prnt=False):
    """
    Finds pages in accordance with the query and remove
    "suppressed" field out of the document.
    All the production servers are chosen by default.
    :param query: pymongo filters, shows, out of which pages
        "suppressed" field should be removed.
    :param servers: required servers. ps1, ps2 and ps3 are
        chosen by default.
    :param prnt: print ids of the pages with removed fields if
        True. False by default.
    :param expanded_prnt: print prepared pages if True.
        False by default.
    return: dict with server names as keys and Cursor objects
        containing pages with removed fields as values.
    """
    servers = normalize_servers_argument(servers)
    rm_suppressed = {}

    if prnt or expanded_prnt:
        print("*" * 100, )
        servers_str = ', '.join([f'ps{id}' for id in servers])
        print('Removing "suppressed" field out of the pages on' 
              f'{servers_str}\n in accordance with following query:')
        pprint.pprint(query)

    for server_id in servers:
        pages = access_to_mongo_collection(
            connect_to_mongo(server_id)
        )
        rm_suppressed[f'ps{server_id}'] = pages.update_many(
            query,
            {'$unset': {'suppressed': True}}
        )

    if prnt or expanded_prnt:
        print('\n' * 2, 'Result:')
        for server_id in servers:
            print("\n" * 4,
                  "~" * 27,
                  "\n" * 2,
                  f'TOTAL REMOVED "suppressed" FIELDS ON ps{server_id}: ',
                  rm_suppressed[f'ps{server_id}'].modified_count,
                  "\n" * 2,
                  "~" * 27,
                  "\n", f'REMOVED "prepared" FIELDS ON ps{server_id}:\n',
                  "~" * 27,
                  "\n" * 2,
                  "-" * 100)
            for page in rm_suppressed[f'ps{server_id}']:
                pprint.pprint(page if expanded_prnt else page['_id'])
                print("-" * 100)
        print("*" * 100, "\n" * 5)

    return rm_suppressed
