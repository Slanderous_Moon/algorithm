import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado.options import define, options

define("port", default=8000, help="run on the given port", type=int)


def retrieve_from_db(widget_id):
    pass

def save_to_db(widget):
    pass


# вымышленный пример
# matched with (r"/widget/(\d+)", WidgetHandler)
class WidgetHandler(tornado.web.RequestHandler):
    def get(self, widget_id):
        widget = retrieve_from_db(widget_id)
        self.write(widget.serialize())

    def post(self, widget_id):
        """Вносит изменения в виджет с соответствующим ID в
            базе данных."""
        widget = retrieve_from_db(widget_id)
        widget['foo'] = self.get_argument('foo')
        save_to_db(widget)

"""
Торнадо поддерживает любой HTTP метод - get, post, put, delete, head, options.
Поведение для любого из этих методов может быть определено в классе 
RequestHandler путём определения одноимённого метода. 
"""

# matched with (r"/frob/(\d+), FrobHandler)
class FrobHandler(tornado.web.RequestHandler):
    def head(self, frob_id):
        frob = retrieve_from_db(frob_id)
        if frob is not None:
            self.set_status(200)
        else:
            self.set_status(404)

    def get(self, frob_id):
        frob = retrieve_from_db(frob_id)
        self.write(frob.serialize())


# HTTP Status codes
"""
Код HTTP-статуса ответа можно установить явно методом set_status() 
класса RequestHandler. Иногда Торнадо будет устанавливать код http-статуса
автоматически. Наиболее частые случаи:

404 Not Found
    Торнадо автоматически вернёт ответ 404, если запрос не соответствует
    ни одному из ассоциированных с обработчиками запросов паттерном.
    
400 Bad Request
    Если get_argument вызывается с аргументом, и соответствующий аргумент
    не будет найден, то Торнадо автоматически вернёт ответ 400.
    
405 Method not Allowed
    Если входящий запрос содержит HTTP-метод, который не определён в классе
    обработчика запросов, Торнадо вернёт ответ 405.
    
500 Internal Server Error
    Торнадо вернёт код 500, когда возникнет любая ошибка, но при этом программа
    продолжит свою работу. Любые необработанные исключения в коде так же 
    вернут код 500.
    
200 OK
    Если запрос был успешным и ни один из других статус-кодов не вернулся, 
    Торнадо вернёт код 200.
    
При возникновении одной из вышеописанных ошибок, по-умолчанию Торнадо 
вернёт клиенту код статуса с коротким сообщением об ошибке. Такие ответы 
по-умолчанию могут быть изменены путём переопределения метода write_error 
в классах обработчиков запросов.
"""


class IndexHandler(tornado.web.RequestHandler):

    def get(self):
        greeting = self.get_argument('greeting', 'Hello')
        self.write(greeting + ' , friendly user!')

    def write_error(self, status_code, **kwargs):
        msg = f'Gosh darnit, user! You caused {status_code} error.'
        self.write(msg)


if __name__ == '__main__':
    tornado.options.parse_command_line()
    app = tornado.web.Application(handlers=[(r"/", IndexHandler)])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
