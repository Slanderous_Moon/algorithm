# basics
# fully functional Tornado app

import tornado
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options

# Каждый аргумент define становится атрибутом глобального объекта
# options.
define('port', default=8000, help='run on the given port', type=int)


# Класс обработчика запросов.
# Каждый из его методов соответствует http-запросам.
class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        # Метод класса RequestHandler get_argument получает
        # аргументы из строки запроса.
        greeting = self.get_argument('greeting', 'Hello')
        # Метод write берёт в качесве аргумента строку и
        # записывает эту строку в Http-ответ.
        self.write(greeting + ' , friendly user!')


if __name__ == '__main__':
    # Парсинг командой строки
    tornado.options.parse_command_line()
    # Создание экземпляра класса Application
    # Параметр handlers - список кортежей. В каждом из кортежей
    # первый агумент - регулярное выражение для сопоставления,
    # а второй - соответствующий обработчик запросов.
    app = tornado.web.Application(handlers=[(r"/", IndexHandler)])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    # Создание экземпляра эвент-лупа, после этого программа готова
    # обрабатывать Http-запросы
    tornado.ioloop.IOLoop.instance().start()


# Приложение доступно по адресу http://localhost:8000/
# Можно настраивать переменую greetings таким образом:
# http://localhost:8000/?greeting=Salutations


