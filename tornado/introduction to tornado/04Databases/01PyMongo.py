import pymongo
import pprint


conn = pymongo.MongoClient('localhost', 27017)

# Можно так же подключиться используя соответствующий URL:
"""
conn = pymongo.MongoClient(
            "mongodb://user:password@staff.mongohq.com:10066/your_mongohq_db")

Данный код подключится к базе данных под названием your_mongohq_db, которая
разв    рнута на MongoHQ, используя юзернейм user и пароль password.
"""

# Создание базы данных
db = conn.example


# Доступ ко всем коллекциям
print(db.list_collection_names())


# Создание коллекции:
widgets = db.widgets


# Вставка в документ:
widget_id = widgets.insert({'foo': 'bar'})
print(db.list_collection_names())

# Вставка:
another_widgets = widgets.insert_one(
    {'name': 'flibnip',
     'description': 'grade-A industrial flibnip',
     'quantity': 3}
)

# Поиск
print(widgets.find_one({'name': 'flibnip'}))

# Монго возвращает словари
doc = widgets.find_one({'name': 'flibnip'})
print(type(doc))
print(doc['name'])
doc['quantity'] = 4

# Изменения в базу данных надо сохранять:
db.widgets.save(doc)

# find возвращает итерируемый объект cursor
for doc in db.widgets.find({'name': 'flibnip'}):
    pprint.pprint(doc)
