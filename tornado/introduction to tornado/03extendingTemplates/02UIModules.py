"""
UI-модули - это объекты, которые инкапулируют разметку, стиль и поведение
включения в шаблоне. Элементы, которые определяются данными модулями обычно
используются несколькими шаблонами или включаются по нескольку раз в один
шаблон.

Модули - это Python-классы, которые наследуются от класса Tornado UIModule
и определяют метод render. Когда шаблон ссылается на модуль при помощи
тэга {% module Foo(...) %}, механизм шаблонов Tornado вызывает метод модуля
render, который возвращает строку, которая заменяет модульный тэг в шаблоне.

UI-модули могут так же включать свои собственные JS и CSS файлы в искомую
страницу или даже перечислять дополнительные JS и CSS ко включению.
Для этого должны быть определены методы embedded_javascript,
embedded_css, javascript_files и css_files.
"""

# Использование UI-модулей.

import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.options

import os.path


from tornado.options import define, options


define("port", default=8000, help="run on the given port", type=int)


class HelloHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('hello.html')


class HelloModule(tornado.web.UIModule):
    def render(self):
        return '<h1>Hello, World!</h1>'


if __name__ == '__main__':
    tornado.options.parse_command_line()
    app = tornado.web.Application(
        handlers=[(r'/', HelloHandler)],
        template_path=os.path.join(os.path.dirname(__file__), 'templatesUI'),
        ui_modules={'Hello': HelloModule}
    )
    server = tornado.httpserver.HTTPServer(app)
    server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
