# Синтаксис шаблонов
import os.path

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options
from tornado.template import Template

define("port", default=8000, help='run on the given port', type=int)


class BookHandler(tornado.web.RequestHandler):
    def get(self):
        self.render(
            "books.html",
            title="Home Page",
            header="Books that are great",
            books=["Learning Python",
                   "Programming Collective Intelligence",
                   "Restful Web Services"]
        )


if __name__ == '__main__':
    content = Template("<html><body><h1>{{ header }}</h1></body></html>")
    print(content.generate(header="Suction!"))

    # В двойные фигурные скобочки можно поместить любой pythonic-код.
    print(Template("{{ 1+1 }}").generate())
    print(Template("{{ 'scrambled eggs'[-4:] }}").generate())

    interpolation = "{{ ', '.join([str(x*x) for x in range(10)]) }}"
    print(Template(interpolation).generate())


"""
Условия и циклы так же могут быть включены в шаблоны Tornado.
Они должны быть обёрнуты в консрукцию {% текст_тут %}:
{% if page is None %} или {% if len(entries) == 3 %}
"""


if __name__ == '__main__':
    tornado.options.parse_command_line()

    app = tornado.web.Application(
        handlers=[(r'/books', BookHandler)],
        template_path=os.path.join(os.path.dirname(__file__), "templates")  # 1
    )

    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

"""
Пример использования python-циклов в html-файлах приведён в файле books.html
"""
