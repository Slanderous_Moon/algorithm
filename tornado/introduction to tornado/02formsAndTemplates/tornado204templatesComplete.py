import os.path
import random

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options


define("port", default=8000, help="run on the given port", type=int)


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('index.html')


class MungedPageHandler(tornado.web.RequestHandler):

    @staticmethod
    def map_by_first_letter(self, text):
        mapped = {}
        for line in text.split('\r\n'):
            for word in [x for x in line.split(' ') if len(x) > 0]:
                if word[0] not in mapped:
                    mapped[word[0]] = []
                mapped[word[0]].append(word)
        return mapped

    def post(self):

        source_text = self.get_argument('source')
        text_to_change = self.get_argument('change')
        source_map = self.map_by_first_letter(self, source_text)
        change_lines = text_to_change.split('\r\n')

        self.render('munged.html',
                    source_map=source_map,
                    change_lines=change_lines,
                    choice=random.choice)


if __name__ == '__main__':
    tornado.options.parse_command_line()
    app = tornado.web.Application(
        handlers=[(r'/', IndexHandler),
                  (r'/poem', MungedPageHandler)],
        template_path=os.path.join(
            os.path.dirname(__file__),
            "templatesMunger"
        ),
        static_path=os.path.join(os.path.dirname(__file__), "staticMunger"),
        debug=True
    )
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

"""
Параметр static_path обозначает папку, где приложение будет хранить все
свои статические ресурсы (изображения, CSS-файлы, файлы JavaScript...)

debug=true в конструкторе приложения - это очень удобно во время разработки,
поскольку Tornado перезапускает сервер при каждом изменении исходного 
Python-файла, однако на продакшене такое не годится, поскольку в данном 
режиме не работает кеширование.
"""

# Статические файлы:
"""
При написании веб-приложения часто хочется обрабатывай статический контент
(CSS-файлы, картинки, JavaScript-файлы) 
не прибегая к написанию обработчика запросов под каждый файл.
Определённый функционал для решения данной задачи у Торнадо есть:

Установление переменной static_path
    Статические файлы будут браться из конкретного места файловой системы.
    Пример выше в коде. Приложение будет отвечать на запросы вроде 
    /static/filename.ext, считывая его из папки static и возвращая в теле
    ответа.
    
Создание статического url при помощи переменной static_url.
    Модуль шаблонов Tornado поддерживает функцию static_url, которая генерирует
    URL для файлов в статической папке. 
    Пример из index.html:
        <link rel="stylesheet" href="{{ static_url('style.css") }}">
    Данный вызов static_url сгенерирует что-то подобное:
        <link rel="stylesheet" href="/static/style.css?v=ab12">

"""