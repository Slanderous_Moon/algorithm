# Использование функций в шаблонах
"""
В Торнадо по-умолчанию доступны несколько функций для шаблонов.

escape(s)
    Заменяет &, <, > в строке s соответствующими HTML сущностями.

url_escape(s)
    Использует urllib.quote_plus для замены символов в строке s
    URL-закодированными эквивалентами.

json_encode(val)
    Кодирует объект val в формате JSON. Фактически является простым
    обращением к методу json.dumps из библиотеки json.

squeeze(s)
    Фильтрует строку s, заменяя последовательности из множества пробелов
    одним-единственным пробелом.
"""

# Использование самописных функций:

from tornado.template import Template


def disemvowel(s):
    return ''.join([x for x in s if x not in 'aeiou'])


if __name__ == '__main__':
    print(disemvowel('George'))

    print(Template("my name is {{d('mortimer')}}").generate(d=disemvowel))
