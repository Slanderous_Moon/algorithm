import os.path

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options

'''
Poem Maker Pro - веб-приложение, которое предлагает пользователю заполнить
HTML-форму, а затем подставляет полученные слова в шаблон поэмы.
'''

define("port", default=8000, help='run on the given port', type=int)


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('index.html')


class PoemPageHandler(tornado.web.RequestHandler):
    def post(self):
        noun1 = self.get_argument('noun1')
        noun2 = self.get_argument('noun2')
        verb = self.get_argument('verb')
        noun3 = self.get_argument('noun3')
        self.render('poem.html',
                    roads=noun1,
                    wood=noun2,
                    made=verb,
                    difference=noun3)


if __name__ == '__main__':
    tornado.options.parse_command_line()

    app = tornado.web.Application(
        handlers=[(r'/', IndexHandler),
                  (r'/poem', PoemPageHandler)],
        template_path=os.path.join(os.path.dirname(__file__), "templates")  # 1
    )

    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


'''
Структрно данное приложение ничем не отличнается от примеров, которые были 
в предыдущей главе. Определены несколько классов обработчиков запросов, которые
впоследствии были переданы объекту tornado.web.Application. 
Ключевое отличие состоит в том, что методу __init__ объекта Application
передан параметр template_path (1). Данный параметр сообщает Торнадо, где
искать файлы шаблонов (HTML-файлы, которые позволяют встраивать кусочки 
Python-кода).
Как только мы сообщим торнадо, где искать файлы шаблонов, мы можем использовать
метод render класса RequestHandler. Данный метод заставляет Tornado прочесть
файл шаблона, заполнить пробелы и отправить результаты в браузер.
'''
