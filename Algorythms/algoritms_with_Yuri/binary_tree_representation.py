from Algorythms.algoritms_with_Yuri.stack import Stack


class BinaryTreeNode:

    def __init__(self, key=None):
        self.key = key
        self.parent = None
        self.left_child = None
        self.right_child = None

    def root(self):
        """
        Процедура поиска корня дерева.
        :return: root element of the tree
        """
        cursor = self
        while cursor.parent is not None:
            cursor = cursor.parent
        return cursor

    def insert(self, key):
        """
        Процедура вставки нового ключа в дерево. Сначала происходит
        "восхождение" до корня дерева, затем вызывается рекурсивная
        процедура __insertion непосредственно вставки нового ключа в
        дерево в соответствии с основным свойством бинарных деревьев
        поиска.
        :param key: key to insert
        """
        cursor = self.root()
        cursor.__insertion(key)

    def __insertion(self, key):
        """
        Рекурсивная процедура вставки ключа в дерево в соответсвии с
        основным свойством бинарных деревьев, текущий элемент
        принимается за корень дерева. Вспомогательная процедура -
        является рекурсивной частью процедуры insert.
        :param key: key to insert
        """
        cursor = self
        if cursor.key is None:
            cursor.key = key
        else:
            if key >= cursor.key:
                if cursor.right_child is None:
                    cursor.right_child = BinaryTreeNode()
                    cursor.right_child.parent = cursor
                cursor.right_child.__insertion(key)
            else:
                if cursor.left_child is None:
                    cursor.left_child = BinaryTreeNode()
                    cursor.left_child.parent = cursor
                cursor.left_child.__insertion(key)

    def tree_search_recursive(self, key):
        """
        Операция поиска узла с заданным ключом.
        :param key: key of the node to be found
        :return: required node or none
        """
        cursor = self.root()
        return cursor.__search_procedure_recursive(key)

    def __search_procedure_recursive(self, key):
        """
        Рекурсивная процедура поиска в дереве, при которой текущий
        элемент считается за корневой. Вспомогательная процедура -
        является рекурсивной частью процедуры tree_search.
        :param key: key of the node to be found
        :return: required node or None
        """
        cursor = self
        if cursor.key is None:
            return None
        elif cursor.key == key:
            return cursor
        elif key < cursor.key:
            if cursor.left_child:
                return cursor.left_child.__search_procedure_recursive(key)
            return None
        else:
            if cursor.right_child:
                return cursor.right_child.__search_procedure_recursive(key)
            return None

    def tree_search_iterative(self, key):
        """
        Итеративный вариант процедуры поиска узла по ключу в дереве.
        """
        cursor = self.root()
        if cursor.key is None:
            return None
        while cursor is not None and key != cursor.key:
            if key < cursor.key:
                cursor = cursor.left_child
            else:
                cursor = cursor.right_child
        return cursor

    def tree_minimum(self):
        """
        Поиск элемента с минимальным ключом в дереве.
        Работает благодаря основному свойству дерева.
        """
        cursor = self.root()
        while cursor.left_child is not None:
            cursor = cursor.left_child
        return cursor

    def tree_maximum(self):
        """
        Поиск элемента с минимальным ключом в дереве.
        Работает благодаря основному свойству дерева.
        """
        cursor = self.root()
        while cursor.left_child is not None:
            cursor = cursor.left_child
        return cursor

    def inorder_tree_walk(self):
        """
        Процедура центрированного обхода элементов дерева.
        """
        cursor = self.root()
        cursor.__inorder_tree_walk_procedure()

    def __inorder_tree_walk_procedure(self):
        """
        Рекурсивная часть процедуры центрированного обхода дерева,
        текущий элемент принимается за корневой.
        """
        cursor = self
        if self.key is not None:
            if cursor.left_child is not None:
                cursor.left_child.__inorder_tree_walk_procedure()
            print(cursor.key)
            if cursor.right_child is not None:
                cursor.right_child.__inorder_tree_walk_procedure()

    def inorder_tree_walk_iterative_stack(self):
        """
        Итеративная процедура центрированного обхода дерева с
        использованием стека. Начинаем от корня, и спускаемся от него
        налево "до упора", добавляя все пройденные элементы в стек
        обработки. Затем итеративно удаляем последний элемент стека,
        выводя его значение на экран, и проверяя, есть ли у него "спуск
        направо". В случае если спуск направо есть, спускаемся налево
        до упора, добавляя все найденные узлы в стек. Продолжаем обрабатывать
        стек.
        """
        cursor = self.root()
        processing_stack = Stack()
        cursor.__descent_to_left_pushing(processing_stack)
        while processing_stack:
            cursor = processing_stack.pop()
            print(cursor.key)
            if cursor.right_child is not None:
                cursor = cursor.right_child
                cursor.__descent_to_left_pushing(processing_stack)

    def __descent_to_left_pushing(self, processing_stack):
        """
        Процедура спуска "налево до упора" от текущего узла дерева,
        с добавлением всех пройденных элементов в стек.
        Второстепенная, используется как часть итеративной процедуры
        центрированного обхода дерева с использованием стека.
        :param processing_stack: стек обработки узлов дерева
        """
        cursor = self
        processing_stack.push(cursor)
        while cursor.left_child is not None:
            cursor = cursor.left_child
            processing_stack.push(cursor)

    def inorder_tree_walk_iterative_stackless(self):
        pass

    def preorder_tree_walk(self):
        pass

    def postorder_tree_walk(self):
        pass


if __name__ == '__main__':

    def print_node(node):
        if isinstance(node, BinaryTreeNode):
            print(node.key, node.parent, node.left_child, node.right_child)
        else:
            print(node)

    node = BinaryTreeNode(5)
    print_node(node)

    node.insert(6)
    node.insert(4)

    for element in (node, node.left_child, node.right_child):
        print_node(element)
    for elem in range(3, 9):
        node.insert(elem)

    print_node(node.tree_search_recursive(3))
    print_node(node.tree_search_recursive(1))

    print_node(node.tree_search_iterative(3))
    print_node(node.tree_search_iterative(1))

    node.inorder_tree_walk()
    node.inorder_tree_walk_iterative_stack()
