
STACK_DEFAULT_MAX_LENGTH = float('inf')


class Stack:
    """
    Старая реализация стека, полугодовой давности.
    Требует проверки и допиливания.
    """
    def __init__(self, max_len=STACK_DEFAULT_MAX_LENGTH):
        self.values = []
        self.max_len = max_len

    def __bool__(self):
        if self.values:
            return True
        else:
            return False

    def __len__(self):
        return len(self.values)

    def __str__(self):
        return str(self.values)

    def __repr__(self):
        return f'Stack object: {self.values}'

    def push(self, val):
        if len(self.values) < self.max_len:
            self.values.append(val)
        else:
            raise StackOverflow

    def pop(self):
        if self.values:
            return self.values.pop()
        else:
            raise StackUnderflow

    def is_empty(self):
        return not self.__bool__()


class StackOverflow(Exception):
    pass


class StackUnderflow(Exception):
    pass

