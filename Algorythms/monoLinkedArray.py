class Node:
    def __init__(self, value=None):
        self.value = value
        self.next = None
        self.is_null = (value is None)

    def push(self, value):
        if self.is_null:
            self.value = value
        else:
            new_node = Node(value)
            current = self
            while current.next:
                current = current.next
            current.next = new_node

    def first_found_index(self, element):
        i = 0
        current = self
        while current.value != element:
            if not current.next:
                return None
            i += 1
            current = current.next
        return i

    def __getitem__(self, index):
        current = self
        for i in range(index + 1):
            if i == index:
                return current
            current = current.next
        raise IndexError('Index out of range')

    def __len__(self):
        if self.is_null:
            return 0
        else:
            current = self
            i = 1
            while current.next:
                current = current.next
                i += 1
            return i

    def insert(self, value, index):
        new_node = Node(value)
        if index != 0:
            self[index - 1].next, new_node.next = new_node, self[index] 
        else:
            new_node.next = self[index]

    def delete(self, index):
        if index == 0:
            raise Exception('Удалить возможно только элементы после корневого.')
        else:
            self[index - 1].next = self[index].next
            

if __name__ == '__main__':
    head = Node(9)
    head.push(2)
    head.push(3)
    head.push(11)
    head.push(14)
    head.push(8)
    head.insert(7, 2)
    head = head.next
    print(head[2].value)
    head.delete(1)
    print(head[1].value)
    head.delete(0)
    print(head[0].value)
