# Пирамиды
'''
Пирамида (binary heap) - это структура данных, представляющая 
собой объект-массив, который может рассматриваться как полное бинарное 
дерево. Каждый узел дерева соответствует определённому элементу
массива. На всех уровнях до последнего дерево полностью заполнено,
последний уровень заполняется до тех пор, пока в масссиве не закончатся
элементы.
У представляющего пирамиду массива A два атриббута - length, количество
элементов массива, а так же heap_size, то есть количество элементов 
пирамиды, содержащихся в массиве A. Ни один из элементов, следующих
после A[heap_size[A]], элементом пирамиды не является.
'''

# Операции вычисления индексов родительского, левого дочернего
# и правого дочернего узлов.

def parent(i):
    return (i - 1) // 2

def left(i):
    return 2*i + 1

def right(i):
    return 2*i + 2

'''
Виды бинарных пирамид: неубывающие и невозрастающие.
Значения, расположенные в узлах, удовлетворяют свойству пирамиды.

Свойство невозрастающих пирамид (max-heap property): 
A[parent(i)] >= A[i]

Свойство неубывающих пирамид (min-heap):
A[parent(i)] <= A[i]

Высота узла пирамиды - число рёбер в самом длинном простом нисходящем
пути от этого узла к какому-то из листьев дерева. Высота пирамиды - это
высота корня пирамиды. 

Высота пирамиды равна O(lg(n)). Время выполнения 
'''

# Поддержка свойств пирамиды

def max_heapify(lst, i, heap_size = None):
    '''
    Процедура "кучкования" узла пирамиды с индексом i. 
    Значение вершины i обменивается с наибольшим значением из
    дочерних узлов, затем процедура рекурсивно вызывается для 
    дочернего узла, с которым произошла перестановка.
    '''
    if heap_size == None:
        heap_size = len(lst) - 1

    left_node_ind = left(i)
    right_node_ind = right(i)

    if left_node_ind <= heap_size and lst[left_node_ind] > lst[i]:
        largest = left_node_ind    
    else:
        largest = i

    if right_node_ind <= heap_size and lst[right_node_ind] > lst[largest]:
        largest = right_node_ind

    if largest != i:
        lst[i], lst[largest] = lst[largest], lst[i]
        max_heapify(lst, largest)


# Преобразование массива lst в невозрастающую пирамиду
# при помощи max_heapify

def build_max_heap(lst, heap_size = None):
    
    if heap_size == None:
        heap_size = len(lst) - 1
    
    for i in range(heap_size // 2, -1, -1):
        max_heapify(lst, i, heap_size)

def heap_sort(lst):

    heap_size = len(lst) - 1

    build_max_heap(lst, heap_size)

    for i in range(len(lst) - 1, 0, -1):

        lst[0], lst[i] = lst[i], lst[0]
        heap_size -= 1
        max_heapify(lst, 0, heap_size)


if __name__ == '__main__':
    import random
    for i in range(10):
        lst = [random.randint(-100, 100) for x in range(20)]
        print('list: ', lst)
        print('\n')
        build_max_heap(lst)
        print('sorting...\n')
        heap_sort(lst)
        print(lst, '\n' * 3)


