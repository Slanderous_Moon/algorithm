# Попытка реализовать наглядный вывод бинарного дерева. 
'''
Задача: реализовать вывод бинарного дерева в "удобной" для восприятия
форме, иными словами, в виде пирамиды, используя функцию print().
Узел дерева должен представлять из себя кортеж, где первый элемент - 
индекс узла, а второй - его значение. 
'''
# Рисование "пирамидки"
'''
Решение упрощённой задачи. Нарисовать бинарное дерево высотой h,
с максимальным числом элементов на первом уровне дерева. Каждый 
узел будет обозначен символом 0.
'''

import math


'''
def count_side_symbols(height):

    if height == 1:
        return 0
    else:
        return count_side_symbols(height - 1) * 2 + 1

def count_separator_symbols(height):
    return count_side_symbols(height) * 2 + 1
'''

def side_symbols_lst(height):
    '''
    Подсчёт количествa символов перед первым элементом и 
    после последнего элемента в каждой строке. Возвращает
    список значений от первого уровня к последнему. 
    '''
    if height == 1:
        return [0]
    else:
        temp_lst = side_symbols_lst(height - 1)
        return temp_lst + [temp_lst[-1] * 2 + 1]


def sep_symbols_lst(side_symbols_lst):
    return [(x * 2 + 1) for x in side_symbols_lst]


def draw_pyramid_ver_1(height):
    side_symbols = side_symbols_lst(height)
    sep_symbols = sep_symbols_lst(side_symbols)
    height -= 1

    for i in range(height, -1, -1):
        sep_quantity = 2 ** (height - i) - 1
        middle_str = (('0' + ' ' * sep_symbols[i]) * sep_quantity + '0')
        res = ' ' * side_symbols[i] + middle_str + ' ' * side_symbols[i]
        print(res)


if __name__ == '__main__':
    print(side_symbols_lst(3), sep_symbols_lst(side_symbols_lst(3)))
    draw_pyramid_ver_1(7)


# Решение задачи для произвольных элементов.
'''
Было сильно усложнено поиском рекурсивного соотношения
для элементов с чётным количеством символов в строковом
представлении. 
Поначалу было решено использовать костыль и добавлять к 
строковому представлению один пробел либо символ-разделитель,
но это действие бы привело к увеличению разделителей на 
самом первом уровне, что противоречило бы условию задачи
и портило бы однородность и эстетику представления деревьев.
'''

def odd_clothing_count_lst(height, len_el):
    
    if height == 1:
        return [(0, 0)]
    else:
        temp_lst = odd_clothing_count_lst(height - 1, len_el)
        this_level_clothing = temp_lst[-1][0] * 2 + len_el // 2 + 1
        return temp_lst + [(this_level_clothing, this_level_clothing)]


def even_clothing_count_lst(height, len_el):

    if height == 1:
        return [(0, 0)]
    elif height == 2:
        temp_lst = even_clothing_count_lst(height - 1, len_el)
        left_clothing = len_el // 2 + 1
        right_clothing = left_clothing - 1
        return temp_lst + [(left_clothing, right_clothing)]
    else:
        temp_lst = even_clothing_count_lst(height - 1, len_el)
        left_clothing = temp_lst[-1][0] * 2 + len_el//2
        right_clothing = left_clothing - 1
        return temp_lst + [(left_clothing, right_clothing)]


def clothing_count_lst(height, len_el):
    '''
    Возвращает список кортежей, в которых первое значение - это
    количество символов-разделителей перед первым элементом на 
    данном уровне дерева, а последнее - соответственно после последнего
    элемента.
    '''
    if len_el % 2 == 1:
        return odd_clothing_count_lst(height, len_el)
    else:
        return even_clothing_count_lst(height, len_el)


def sep_count_lst(clothing_count_lst):
    return [clothing[0] + clothing[1] + 1 for clothing in clothing_count_lst]


def draw_pyramid_ver_2(height, element, sep_symbol):
    rep_el = str(element)
    len_el = len(rep_el)

    clothing_lst = clothing_count_lst(height, len_el)
    sep_lst = sep_count_lst(clothing_lst)

    height -= 1

    for i in range(height, -1, -1):
        sep_quantity = 2 ** (height - i) - 1
        separator = sep_symbol * sep_lst[i]
        left_clothing = sep_symbol * clothing_lst[i][0]
        right_clothing = sep_symbol * clothing_lst[i][1]

        middle_str = (rep_el + separator) * sep_quantity + rep_el
        res = left_clothing + middle_str + right_clothing
        print(res)

if __name__ == "__main__":
    draw_pyramid_ver_2(5, (1, 1), ' ')
    draw_pyramid_ver_2(5, '000', ' ')


# Дальнейшее усложнение задачи. Представление списка в виде дерева.


def count_max_len(lst):
    '''
    Вычисляет наибольшую длину строкового 
    представления элементов в списке.
    '''
    res = 0
    for element in lst:
        if len(str(element)) > res:
            res = len(str(element))
    return res


import math

def count_levels(n):
    '''
    Подсчитывает количество уровней в дереве в зависимости от
    количества узлов дерева.
    '''
    res = math.log(n + 1, 2)
    if res % 1:
        return int(res) + 1
    else:
        return int(res)


def count_repr_symbols(height, element_gap):
    '''
    Подсчитывает количество символов, которыми можно каждый 
    уровень дерева высотой height, с длиной строкового 
    представления элементов element_gap. Расчёт ведётся
    по самому нижнему уровню.
    '''
    elements_num = 2 ** (height - 1)
    sep_count = elements_num - 1

    return elements_num * element_gap + sep_count




def lst_tree_repr(lst, sep_symbol = ' '):
    
    if(len(sep_symbol) != 1):
        raise Exception("Паша хуй соси")

    max_len_el = count_max_len(lst)     # Подсчёт наибольшей длины среди элементов
    height = count_levels(len(lst))     # Подсчёт высоты конечного дерева
    level_str_len = count_repr_symbols(height, max_len_el)  # Длина строки представления

    clothing_lst = clothing_count_lst(height, max_len_el)
    sep_lst = sep_count_lst(clothing_lst)
    clothing_lst = clothing_lst[::-1]
    sep_lst = sep_lst[::-1]

    res = ''

    symbols_count = 0       # Cчётчик символов, уже добавленных в строку
    elements_count = 0      # Счётчик элементов, уже добавленных в строку
    str_count = 0           # Счётчик строк, которые уже добавлены в результат

    current_str = ''        # 'Рабочая строка'

    for i in range(len(lst)):
        
        added_element = f'{lst[i]: ^{max_len_el}}'
        
        current_str += added_element
        elements_count += 1
        symbols_count += max_len_el

        # На данном шаге проверка количества символов не нужна

        max_number_of_elements = 2 ** str_count

        if elements_count == max_number_of_elements:
            # Добавление символов по бокам ("одежды") и завершающего
            clothing = clothing_lst[str_count]
            current_str = sep_symbol * clothing[0] + current_str + sep_symbol * clothing[1]
            symbols_count += clothing[0] + clothing[1]
            current_str += '\n'
            

            # Добавление текущей рабочей строки к результату,
            # переход к новой рабочей строке
            res += current_str

            str_count += 1
            elements_count = 0
            symbols_count = 0
            current_str = ''

        elif i == len(lst) - 1:
            # Данная ситуация может произойти только на самом нижнем уровне.
            # По этой причине "одежда" слева не добавляетя.
            current_str += sep_symbol * (level_str_len - symbols_count)
            current_str += '\n'
            res += current_str

        else:
            # Добавление разделителя
            sep = sep_symbol * sep_lst[str_count]
            current_str += sep
            symbols_count += sep_lst[str_count]

    return res

if __name__ == "__main__":
    print('\n' * 15)
    print('lst_tree_repr test')
    print('\n' * 3)
    import random
    lst = [random.randint(-1000, 1000) for x in range(41)]
    print(lst)
    print('\n' * 3)
    print(lst_tree_repr(lst))

    print('\n' * 15)
    print('lst_tree_repr test')
    print('\n' * 3)
    import random
    lst = [random.randint(-999, 1000) for x in range(31)]
    print(lst)
    print('\n' * 3)
    lst.sort(reverse=True)
    print(lst_tree_repr(lst))







    

