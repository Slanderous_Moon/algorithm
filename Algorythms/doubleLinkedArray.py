'''
Реализация двусвязного циклического списка с ограничителем.
'''

class Node:
    def __init__(self, value = None):
        self.value = value
        self.prev = self
        self.next = self

    def __bool__(self):
        if (self.value == None and
            self.prev.value == None and
            self.next.value == None):
            return False
        else:
            return True

    def append_to_tail(self, val):
        '''
        Операция добавления в конец двусвязного списка.
        '''
        new_node = Node(val)
        current, previous = self, self.prev
        (current.prev, previous.next, 
        new_node.prev, new_node.next) = (new_node, new_node,
                                        previous, current)

    def append_to_head(self, val):
        '''
        Операция добавления в начало двусвязного списка.
        '''
        new_node = Node(val)
        current, next = self, self.next
        (current.next, next.prev,
        new_node.prev, new_node.next) = (new_node, new_node,
                                            current, next)

    def insert(self, val, index):
        '''
        Операция вставки.
        '''
        pass


if __name__ == "__main__":
    a = Node()
    print(a.value)
    print(a.next.value)
    print(a.next.next.value)
    print(a.prev.value)
    print(a.prev.prev.value)

    print('\n' * 3 + 'Добавление в конец.')
    a.append_to_tail(1)
    print(a.value)
    print(a.next.value)
    print(a.next.next.value)
    print(a.prev.value)
    print(a.prev.prev.value)

    a.append_to_head(2)
    print(a.value)
    print(a.next.value)
    print(a.next.next.value)
    print(a.prev.value)
    print(a.prev.prev.value)