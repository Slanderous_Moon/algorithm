class StackOverflow(Exception): pass
class StackUnderflow(Exception): pass

class Stack:
    def __init__(self, max_len = float('inf')):
        self.values = []
        self.max_len = max_len

    def __bool__(self):
        '''
        Эквивалент is_empty из Кормена.
        '''
        if self.values:
            return True
        else:
            return False

    def __len__(self):
        return len(self.values)

    def __str__(self):
        return str(self.values)

    def __repr__(self):
        return f'Stack object: {self.values}'

    def push(self, val):
        if len(self.values) < self.max_len:
            self.values.append(val)
        else:
            raise StackOverflow

    def pop(self):
        if self.values:
            return self.values.pop()
        else:
            raise StackUnderflow

    def is_empty(self):
        return not self.__bool__()


class QueueUnderflow(Exception): pass
class QueueOverflow(Exception): pass
    
class Queue:
    def __init__(self, max_len = float('inf')):
        self.values = []
        self.max_len = max_len

    def __bool__(self):
        if self.values:
            return True
        else:
            return False
    
    def __len__(self):
        return len(self.values)

    def __str__(self):
        return str(self.values)

    def __repr__(self):
        return f'Queue object: {self.values}'

    def enqueue(self, val):
        if len(self.values) < self.max_len:
            self.values.append(val)
        else:
            raise QueueOverflow

    def dequeue(self):
        if self.values:
            return self.values.pop(0)
        else:
            raise QueueUnderflow

    
class DequeOverflow(Exception): pass
class DequeUnderflow(Exception): pass

class Deque:
    def __init__(self, max_len = float('inf')):
        self.values = []
        self.max_len = max_len

    def __bool__(self):
        if self.values:
            return True
        else:
            return False

    def __len__(self):
        return len(self.values)

    def __str__(self):
        return str(self.values)

    def __repr__(self):
        return f'Deque object: {self.values}'

    def append_to_head(self, val):
        if len(self.values) < self.max_len:
            self.values.insert(0, val)
        else:
            raise DequeOverflow

    def append_to_tail(self, val):
        if len(self.values) < self.max_len:
            self.values.append(val)
        else:
            raise DequeOverflow

    def dequeue(self):
        if self.values:
            return self.values.pop(0)
        else:
            raise DequeUnderflow

    def pop(self):
        if self.values:
            return self.values.pop()
        else:
            raise DequeUnderflow

    


if __name__ == "__main__":
    import random

    print('Stack experiments:')
    stack_1 = Stack()
    print(stack_1)
    for _ in range(10):
        stack_1.push(random.randint(-10000, 10000))
    print(repr(stack_1))
    for _ in range(len(stack_1) // 2):
        print(stack_1.pop())
    print(stack_1)

    print('\n' * 3 + 'Queue experiments:')
    queue_1 = Queue()
    print(queue_1)
    for _ in range(10):
        queue_1.enqueue(random.randint(-10000, 10000))
    print(repr(queue_1))
    for _ in range(len(queue_1) // 2):
        print(queue_1.dequeue())
    print(queue_1)
