# Вероятностный анализ

'''
Вероятностный анализ - это анализ задачи, при котором используются
вероятности тех или иных событий. Чаще всего используется при
определении времени работы алгоритма.

Алгоритм называется рандомизированным, если его поведение 
определяется не только набором входных величин, но и значениями,
которые выдаёт генератор случайных чисел.

Большинство сред программирования предоставляют генератор 
псевдослучайных чисел, то есть детерминированный алгоритм, возвращающий 
числа, которые ведут себя при статистическом анализе как случайные.
'''

# Упражнение 5.1-2.
# Реализация random(a,b) на основе random(0,1)

import random

def random_int(start, stop):
    '''
    Генерирует случайное целочисленное значение в 
    промежутке между start и stop. start <= stop
    '''
    movement = random.randint(0, 1)

    if stop - start == 0:
        return start
    elif stop - start == 1:
        if movement:
            return stop
        else:
            return start
    else:
        divider = start + (stop - start) // 2
        if movement:
            return random_int(divider + 1, stop)
        else:
            return random_int(start, divider)

if __name__ == '__main__':
    quantities = {x: 0 for x in range(1, 6)}
    for i in range(100000):
        quantities[random_int(1, 5)] += 1
    print(quantities)

'''
def random_int(start, stop):
    difference = stop - start
    n = 0
    res = start
    while difference != 1:
        difference = difference // 2
        n += 1
    for i in range(n + 1):
        res += random.randint(0, 1) * (2 ** i)
    return res
'''

# 5.2 Индикаторная случайная величина.
'''
Индикаторная случайная величина I{A}, связанная с событием А,
определяется как 1, если событие А произошло, или как 0, если
событие А не произошло.
'''

