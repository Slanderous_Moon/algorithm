# 2.1 Сортировка вставкой

'''
Задача сортировки.
Вход: последовательность из n чисел [a1, a2.., an]
Выход: перестановка (изменение порядка) [a'1, a'2, .. , a'n], где
a'1<=a'2<=..<=a'n
Ключи (keys) - сортируемые числа
'''

# insertion sort, сортировка вставкой


def insertion_sort(lst):
    for j in range(1, len(lst)):
        key = lst[j]
        i = j - 1
        while i >= 0 and lst[i] > key:
            lst[i + 1] = lst[i]
            i -= 1
        lst[i + 1] = key


if __name__ == '__main__':
    print('insertion sort, сортировка вставкой', '\n'*2)
    lst = [45, 23, 44, 24, 199]
    insertion_sort(lst)
    print(lst)
    print('\n' * 3)

    lst = [45, 23, 44, 24, 199, 1, 2, 3]
    insertion_sort(lst)
    print(lst)
    print('\n' * 3)


'''
Индекс j указывает текущее значение, которое помещается в ячейку key.
В начале каждой итерации внешнего цикла for с индексом j список lst
состоит из 2 частей. Элементы lst[0, ..., j - 1] отсортированы,
а элементы lst[j + 1, ..., n] - ещё нет. При этом элементы lst[0,.., j - 1]
изначально тоже находились на позициях от 0 до j - 1, но в другом,
неотсортированном порядке.
Данное свойство элементов lst[0..j-1] называется инвариантом цикла.

В начале каждой итерации цикла for подмассив lst[0..j-1] содержит
те же элементы, которые были в нём с самого начала, но расположенные в
отсортированном порядке.

Инварианты обладают следующими свойствами:
1) Инициализация. Они справедливы перед первой инициализацией цикла.
2) Сохранение. Если они истинны перед очередной итерацией цикла, то
остаются истинны и после неё.
3) Завершение. По завершении цикла инварианты позволяют убедиться в
правильности алгоритма.
'''

# Самостоятельная работа. Задача поиска.
'''
Вход: последовательность n чисел A = [a1, a2,.., an], величина v
Выход: индекс i, обладающий свойством v = A[i], или значение Nil
если в массиве A отсутствует значение v.
'''


def find_value(lst, val):
    res = None
    for i in range(len(lst)):
        if val == lst[i]:
            res = i
    return res


if __name__ == '__main__':
    print('Самостоятельная работа. Задача поиска.', '\n'*2)
    print(find_value([1, 2, 3], 2))
    print(find_value([1, 2, 3, 4], 5))
    print('\n' * 3)

# Самостоятельная работа. Задача сложения двоичных целых чисел.
'''
Вход: два списка из символов 1 и 0 длины n, представляющие собой двоичные
записи целых чисел.
Выход: список из символов 1 и 0 длины n + 1, представляющий собой
двоичную запись суммы двух целых чисел.
'''


def bit_add(a, b):
    '''
    Функция сложения двух однозначных двоичных чисел.
    Возвращает кортеж, где первое значение - это количество десятков,
    образованных в результате операции сложения, а второе - количество
    единиц.
    '''
    if a == '0' and b == '0':
        return ('0', '0')
    elif a == '1' and b == '1':
        return ('1', '0')
    else:
        return ('0', '1')


def bin_add(a, b):
    res = ['0'] * (len(a) + 1)
    for i in range(len(a) - 1, -1, -1):
        temp = bit_add(a[i], b[i])
        (res[i], res[i+1]) = (bit_add(bit_add(temp[1], res[i+1])[0], temp[0])[1],
                                bit_add(temp[1], res[i+1])[1])
    return res


if __name__ == '__main__':
    print('Задача сложения двоичных целых чисел.', '\n'*2)
    a = ['0', '0', '0', '0', '1', '1']
    b = ['0', '1', '0', '1', '1', '1']
    print('a: ', a, '\n', 'b: ',  b)
    print(bin_add(a, b))
    print('\n' * 3)


# 2.2 Анализ.
'''
Анализ алгоритма заключается в предсказании требуемых для его выполнения
ресурсов, а именно: памяти, пропускной сети, необходимого аппаратного
обеспечения и времени вычисления.

Алгоритмы реализуются в виде программ. В качестве технологии реализации
принята модель обобщеённой однопроцессорной машины с памятью с
произвольным доступом (Random-Access Machine - RAM). В модели команды
выполняются последовательно, одновременно выполняемые операции
отсутствуют.

Размер входных данных - это количество входных элементов либо общее
количество битов, необходимых для представления входных данных в обычных
двоичных обозначениях.

Время работы алгоритмы измеряется в количестве элементарных операций
(шагов), которые необходимо выполнить.

Основное внимание определению времени работы уделяется в наихудшем случае
так как:
1) Время работы в наихудшем случае - это верхний предел этой величины при
любых входных данных. Для выполнения алгоритма не потребуется большее
количество времени.
2) В некоторых алгоритмах наихудший случай встречается достаточно часто.
Например, информация может отсутствовать в базе данных.
3) Характер поведения "усреднённого" времени работы ничем не лучше
поведения времени работы в наихудшем случае.

Скорость роста (порядок роста) времени работы - это то, что на самом деле
важно при анализе времени работы. Во внимание будет приниматься
только главный член формулы, поскольку при больших n членами меньшего
порядка можно пренебречь. Постоянными множителями тоже можно пренебречь.
В итоге время работы алгоритма сортировки вставкой в наихудшем случае
равно O(n^2) (тета от n в квадрате).
'''


def selection_sort(lst):
    for i in range(len(lst) - 1):
        min_i = len(lst) - 1
        for j in range(i, len(lst) - 1):
            if lst[j] < lst[min_i]:
                min_i = j
        lst[i], lst[min_i] = lst[min_i], lst[i]


if __name__ == '__main__':
    print('Сортировка выбором.', '\n'*2)
    lst = [5, 4, 3, 2, 1]
    selection_sort(lst)
    print(lst)
    print('\n' * 3)


# 2.3 Разработка алгоритмов.
'''
В алгоритме, работающем по методу вставок, применяется инкрементный
подход: располагая остортированным подмассивом A[1..j-1], элемент A[j]
помещается туда, где он должен находиться, в результате чего получается
отсортированный подмассив A[1..j].
Альтернативный подход к разработке алгоритмов: метод разбиения.
'''

# 2.3.1 Метод декомпозиции.

'''
Многие алгоритмы имеют рекурсивную структуру: для решения задач они
рекурсивно вызывают себя сами один или несколько раз, чтобы решить
вспомогательную задачу, имеющую непосредственное отношение к поставленной
задаче. Разрабатываются такие алгоритмы при помощи декомпозиции
(разбиения): сложная задача дробится на несколько более простых, подобных
исходной, но имеющей меньший объём. Простые задачи решаются рекурсивным
методом, после чего полученные решения комбинируются с целью получить
решение исходной задачи.

Три этапа декомпозиции:
1) Разделение задачи на несколько подзадач.
2) Покорение - рекурсивное решение подзадач. Когда объём подзадачи мал,
выделенные подзадачи решаются непосредственно.
3) Комбинирование решения исходной из вспомогательных задач.
'''

# Сортировка слиянием:
'''
Разделение: сортируемая последовательность из n элементов дробится
на две меньших последовательности по n/2 элементов.
Покорение: сортировка обеих вспомогательных последовательностей методом
слияния.
Комбинирование: слияние двух отсортированных последовательностей для
получения окончательного результата.

Рекурсия достигает своего нижнего предела, когда длина сортируемой
последовательности становится равной 1.
Основная операция, которая производится в процессе сортировки по методу
слияний - объединение двух отсортированных последовательностей в ходе
комбинирования. Делается это при помощи вспомогательной процедуры
Merge(A, p, q, r), где A - массив, p, q и r - индексы, нумерующие элементы
массива, такие, что p <= q < r. Предпрлагается, что элементы подмассивов
A[p..q] и A[q+1..r] упорядочены. Эти два подмассива сливаются в
отсортированный, элементы которого заменяют текущие элементы подмассива
A[p..r].

Для выполнения процедуры Merge тратится время O(n), где n = r - p + 1 -
количество подлежащих слиянию элементов.
'''


def merge(lst, start, divider, stop):
    '''
    Подфункция слияния - часть функции сортировка слиянием.
    Подфункция произвоит сортировочное слияние двух подмассивов
    lst[start: divider+1], lst[divider+1: stop+1].
    '''
    lst1 = lst[start: divider + 1]
    lst2 = lst[divider + 1: stop + 1]
    for k in range(start, stop+1):
        if lst1 and lst2:
            if lst1[0] <= lst2[0]:
                lst[k] = lst1[0]
                lst1 = lst1[1:]
            else:
                lst[k] = lst2[0]
                lst2 = lst2[1:]
        elif lst1:                  # Если вдруг один из листов закончился
            lst[k] = lst1[0]
            lst1 = lst1[1:]
        elif lst2:
            lst[k] = lst2[0]
            lst2 = lst2[1:]


def merge_sort(lst, start=0, stop=None):
    '''
    Головная функция сортировки слиянием. Аргумент start - индекс первого
    элемента списка lst, с которого требуется начать сортировку,
    stop - индекс последнего элемента списка lst, на котором сортировку
    надо прервать.
    '''
    if stop == None:     # Создание служебного значения stop
        stop = len(lst) - 1

    if start < stop:    # это несправедливо только тогда, когда
                        # start == stop, что означает всего один элемент
        divider = (start + stop) // 2
        merge_sort(lst, start=start, stop=divider)
        merge_sort(lst, start=divider + 1, stop=stop)
        merge(lst, start, divider, stop)


if __name__ == '__main__':
    print('Сортировка слиянием', '\n'*2)
    lst = [5, 4, 6, 2, 2, 1, 3]
    merge_sort(lst)
    print(lst)

    print('\n'*3)

    lst = [5, 4, 6, 2, 2, 1, 3]
    merge_sort(lst, 0, 6)
    print(lst)
    print('\n' * 3)


# Самостоятельная работа. Бинарный поиск.

def bool_binary_search(lst: 'sorted', value):
    '''
    Ищет значение в отсортированной последовательности
    методом деления на два, возвращает True в случае, если
    значение нашлось, False если значение отсутствует.
    '''
    if len(lst) == 1:
        if value == lst[0]:
            return True
        else:
            return False
    else:
        divider = len(lst) // 2
        if value >= lst[divider]:
            return bool_binary_search(lst[divider:], value)
        else:
            return bool_binary_search(lst[:divider], value)


if __name__ == '__main__':
    print('Задача бинарного поиска True/False.', '\n'*2)
    lst = list(range(1, 1000, 3))
    print(bool_binary_search(lst, 1), bool_binary_search(lst, 2),
            bool_binary_search(lst, 4))
    print('\n' * 3)


def binary_search(lst, value, index_lst=None):
    '''
    Ищет в отсортированном списке уникальных целых значений индекс
    вхождения значения value. Если не находит, возвращает None.
    '''
    count = len(lst)

    if count == 0:
        return None

    if index_lst == None:    # Создаёт служебный список индексов
        index_lst = list(range(count))

    if count == 1:
        if value != lst[0]:
            return None
        else:
            return index_lst[0]
    else:
        divider = count // 2
        if value >= lst[divider]:
            return binary_search(lst[divider:], value, index_lst[divider:])
        else:
            return binary_search(lst[:divider], value, index_lst[:divider])

def multiple_binary_search(lst, value, index_lst=None):
    '''
    Ищет в списке целых значений индексы
    вхождения значения value. Если не находит, возвращает None.
    Идея говённая, поскольку произойдёт обход всех значений.
    Вот если бы for не существовало... то Python был бы дедушкой.
    '''
    count = len(lst)

    if count == 0:
        return []

    if index_lst == None:    # Создаёт служебный список индексов
        index_lst = list(range(count))

    if count == 1:
        if value != lst[0]:
            return []
        else:
            return [index_lst[0]]
    else:
        divider = count // 2
        return (multiple_binary_search(lst[:divider], value, index_lst[:divider]) + 
                multiple_binary_search(lst[divider:], value, index_lst[divider:]))


if __name__ == '__main__':
    print('Задача ,бинарного поиска.', '\n'*2)
    lst = list(range(1, 1000, 3))
    print(binary_search(lst, 1), binary_search(lst, 2),
            binary_search(lst, 4))
    print('\n' * 3)

    import random
    for i in range(10):
        lst = [random.randint(1, 10) for x in range(10)]
        print(lst, multiple_binary_search(lst, 2))
    print('\n' * 3)


# Самостоятельная работа. Замена while в алгоритме сортировки вставкой
# на бинарный поиск.

def sub_binary_search(lst, val, index_lst=None):
    '''
    Подалгоритм сортировки вставкой.
    Ищет в lst такой индекс i, что
    (lst[i-1]<)val <= lst[i], иначе возвращает
    значение len(lst)
    '''
    if index_lst == None:   # Создание служебного списка индексов
        index_lst = list(range(len(lst)))

    if len(lst) == 1:       # Возможно в двух крайних случаях.
        if val <= lst[0]:   # Искомый val оказался <= всех значений lst
            return 0
        else:               # val оказался > всех значений lst
            return index_lst[0] + 1

    elif len(lst) == 2:
        if lst[0] < val <= lst[1]:  # Искомый индекс найден
            return index_lst[1]
        elif val <= lst[0]:
            return sub_binary_search(lst[:1], val, index_lst[:1])
        else:
            return sub_binary_search(lst[1:], val, index_lst[1:])

    else:
        divider = len(lst) // 2
        if val >= lst[divider]:
            return sub_binary_search(lst[divider:], val, index_lst[divider:])
        else:
            return sub_binary_search(lst[:divider], val, index_lst[:divider])


def insertion_sort_mod2(lst):
    '''
    Обновлённая сортировка вставкой.
    Поиск места вставки теперь реализован как бинарный.
    '''
    for j in range(1, len(lst)):
        key = lst[j]
        i = sub_binary_search(lst[:j], key)
        lst[i+1: j+1] = lst[i: j]
        lst[i] = key


if __name__ == '__main__':
    print('Замена while в алгоритме сортировки вставкой на бинарный поиск.',
            '\n'*2)
    import random
    lst = list(range(21))
    random.shuffle(lst)
    print(lst)
    insertion_sort_mod2(lst)
    print(lst)
    print('\n' * 3)


# Самостоятельная работа. Поиск двух элементов множества S, сумма которых
# дала бы значение x.

def sum_search(given_set, val):
    lst = [x for x in given_set]
    merge_sort(lst)
    for i in range(1, len(lst)):
        if binary_search(lst, val - lst[i]) != None:
            return True
    return False


if __name__ == '__main__':
    print('Поиск двух элементов множества S, сумма которых дала бы значение x.',
            '\n'*2)
    import random
    given_set = set(random.randint(0, 10) for x in range(10))
    x = random.randint(0, 10)
    print(given_set, x, sum_search(given_set, x))
    print('\n' * 3)

    given_set = set(random.randint(0, 10) for x in range(3))
    x = random.randint(0, 10)
    print(given_set, x, sum_search(given_set, x))
    print('\n' * 3)


# Задача 2-2. Сортировка пузырьком.

def bubble_sort(lst):
    for i in range(len(lst)):
        for j in range(len(lst) - 1, i, -1):
            if lst[j] < lst[j-1]:
                lst[j], lst[j-1] = lst[j-1], lst[j]


if __name__ == '__main__':
    print('Задача 2-2. Сортировка пузырьком. Не готова', '\n'*2)
    import random
    lst = [x ** 2 for x in range(10)]
    random.shuffle(lst)
    print(lst)
    bubble_sort(lst)
    print(lst)
    print('\n'*3)


# Задача 2-4. Подсчёт числа инверсий в списке
# со сложностью не более O(n*ln(n)).
'''
Подсчёт числа инверсий в списке чисел. Работает на основе
слегка изменённого merge_sort, а потому имеет сложность
O(n*ln(n)).
Основной подсчёт идёт в подфункции merge_count().

В идеальном случае, когда инверсий в главном списке нет,
элементы главного списка отсортированы от меньшего значения
к большему. Это значит, что все элементы в списке left_lst меньше
либо равны элементам в списке right_lst. Следовательно, каждый
элемент списка left_lst будет удалён прежде, чем будет
удалён хотя бы один из элементов из списка right_lst.

Событие "в списке left_lst остались элементы, но был удалён
элемент списка right_lst" означает, что удалённый элемент списка
right_lst формирует по одной инверсии с каждым из элементов,
оставшихся  в списке left_lst, что увеличивает счётчик
inv_multiplier  количества инверсий каждого из оставшихся
в списке left_lst элементов с уже удалёнными элементами
из списка right_lst.

Каждый раз, когда происходит удаление элемента из списка
left_lst, запускается процедура "сбора урожая", когда
счётчик инверсий counter для данной итерации функции merge_count  
увеличивается на значение счётчика inv_multiplier.
Процедура может "работать впустую", когда значение
cчётчика inv_multiplier было равным 0, что означает, что
удалённый из списка left_lst элемент не формировал инверсий
с элементами списка right_lst.

'''

def inversion_count2(lst):
    # Копирование списка во избежание
    # изменений в исходном.
    copied_lst = lst[:]
    return count_by_merge_sort(copied_lst)


def count_by_merge_sort(lst, start=0, stop=None):

    counter = 0

    if stop == None:
        stop = len(lst) - 1

    if start < stop:
        # Обратное возможно тогда и только тогда, когда
        # start == stop. Сортировка единственного элемента
        # не требуется, поскольку единственный элемент
        # априори отсортирован.
        divider = (start + stop) // 2
        counter += count_by_merge_sort(lst, start=start, stop=divider)
        counter += count_by_merge_sort(lst, start=divider + 1, stop=stop)
        counter += merge_count(lst, start, divider, stop)

    return counter

def merge_count(lst, start, divider, stop):
    '''
    Подфункция "слияния", подсчёт ведётся тут.
    '''
    # Далее merge_count будет обращаться к
    # переменной counter объемлющей функции.
    counter = 0
    
    divider += 1
    stop += 1

    left_lst = lst[start: divider]
    right_lst = lst[divider: stop]

    # Счётчик числа инверсий, которые
    # сформированы у каждого из оставшихся
    # в списке left_lst элементов с уже удалёнными
    # элементами из списка right_lst. 
    inv_multiplier = 0
    
    for k in range(start, stop):
        
        if left_lst and right_lst:
            
            if left_lst[0] <= right_lst[0]:
                lst[k] = left_lst.pop(0)
                counter += inv_multiplier    # "Сбор урожая"
                
            else:
                lst[k] = right_lst.pop(0)    # Триггер увеличения
                inv_multiplier += 1          
                
        elif left_lst:
            lst[k] = left_lst.pop(0)
            counter += inv_multiplier        # "Сбор урожая"
            
        elif right_lst:
            lst[k] = right_lst.pop(0)
    return counter

if __name__ == '__main__':
    print('Задача 2-4. Подсчёт числа инверсий.', '\n'*2)
    import random
    for i in range(10):
        lst = [random.randint(0, 10) for x in range(10)]
        print(lst, inversion_count2(lst))
        print(lst)
    print('\n' * 3)
    
